//package miner.bitcoin.ctrl;
//
//import android.content.Intent;
//import android.graphics.Color;
//import android.support.v4.app.FragmentTransaction;
//
//import com.ramotion.paperonboarding.PaperOnboardingFragment;
//import com.ramotion.paperonboarding.PaperOnboardingPage;
//import com.ramotion.paperonboarding.listeners.PaperOnboardingOnRightOutListener;
//
//import java.util.ArrayList;
//
//import miner.bitcoin.App;
//import miner.bitcoin.R;
//import miner.bitcoin.activity.AuthActivity;
//import miner.bitcoin.activity.MainActivity;
//import miner.bitcoin.helper.AppPreferenceManager;
//
//public class AuthCtrl {
//
//    private AuthActivity authActivity;
//
//    public AuthCtrl(AuthActivity authActivity){
//        this.authActivity = authActivity;
//    }
//
//    public void init(){
//        PaperOnboardingPage scr1 = new PaperOnboardingPage(App.getStringById(R.string.first_onboarding_title),
//                App.getStringById(R.string.first_onboarding),
//                Color.parseColor("#678FB4"), R.drawable.storage, R.drawable.onboarding_pager_circle_icon);
//        PaperOnboardingPage scr2 = new PaperOnboardingPage(App.getStringById(R.string.second_onboarding_title),
//                App.getStringById(R.string.second_onboarding),
//                Color.parseColor("#65B0B4"), R.drawable.miner, R.drawable.onboarding_pager_circle_icon);
//        PaperOnboardingPage scr3 = new PaperOnboardingPage(App.getStringById(R.string.third_onboarding_title),
//                App.getStringById(R.string.third_onboarding),
//                Color.parseColor("#9B90BC"), R.drawable.withdraw, R.drawable.onboarding_pager_circle_icon);
//
//        ArrayList<PaperOnboardingPage> elements = new ArrayList<>();
//        elements.add(scr1);
//        elements.add(scr2);
//        elements.add(scr3);
//
//        PaperOnboardingFragment onBoardingFragment = PaperOnboardingFragment.newInstance(elements);
//
//        FragmentTransaction fragmentTransaction = authActivity.getSupportFragmentManager().beginTransaction();
//        fragmentTransaction.add(R.id.fragment_container, onBoardingFragment);
//        fragmentTransaction.commit();
//
//        onBoardingFragment.setOnRightOutListener(new PaperOnboardingOnRightOutListener() {
//            @Override
//            public void onRightOut() {
//                AppPreferenceManager.getInstance().setNotFirstLaunch();
//
//                Intent intent = new Intent(authActivity, MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                authActivity.startActivity(intent);
//                authActivity.finish();
//            }
//        });
//    }
//}
