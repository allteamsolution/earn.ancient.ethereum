package miner.bitcoin.helper;

import java.util.List;
import java.util.Random;

import miner.bitcoin.BuildConfig;
import miner.bitcoin.provider.FaqProvider;

public class Constants {

    public static final int NOTIFICATION_CODE = 33;
    public static final String PACK = "clientapp.swiftcom.org";

    public static double minPayoutCrypt = 0.15;//500 kSatoshi
    public static double minPayoutEuro = 20;//500 kSatoshi

    public static double satoshiToBtc = 0.01 * 0.001 * 0.001;//1 Satoshi
    public static double btcToSatoshi = 100 * 1000 * 1000;//1 BTC

    public final static String CRYPT = "Ethereum";
    public final static String CRYPT_CODE = "ETH";
    public final static String CRYPT_CODE_PARTS = "finney";

    public final static float referralBonus = 0.00075f;//"cents"

    public static final float DEF_PLAN_REW =  0.0000000126f;
    public static final float PREMIUM_PLAN_REW = 0.0000000252f;
    public static final float PLATINUM_PLAN_REW = 0.00000000378f;

    public static double getReward(){
        return new Random().nextInt(60) + 50;
    }

    public static final String FORMAT_BALANCE = "%.11f";

    public static final float PREMIUM_PRICE = 0.0525f;
    public static final float PLATINUM_PRICE = 0.09f;

    public static int TIME_AD_REWARDS_SEC;
    public static double VIDEO_REWARD = 0.000003;

    static {
        if (BuildConfig.DEBUG) {
            TIME_AD_REWARDS_SEC = 30 * 1000; // 10sec
        } else {
            TIME_AD_REWARDS_SEC = 30 * 60 * 1000;// 30 min
        }
    }


    public static List<FaqProvider.FaqModel> getExtraFaqs(){
        return null;
    }
}
