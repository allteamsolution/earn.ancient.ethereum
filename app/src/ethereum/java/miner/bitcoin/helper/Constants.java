package miner.bitcoin.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import miner.bitcoin.R;
import miner.bitcoin.provider.FaqProvider;

public class Constants {

    public static final int NOTIFICATION_CODE = 34;

    public static double minPayoutCrypt = 0.05;//ETH

    public static double satoshiToBtc = 0.001 * 0.001 * 0.001;//1 Gwei
    public static double btcToSatoshi = 1000 * 1000 * 1000;//1 ETH

    public final static String CRYPT = "Ethereum";
    public final static String CRYPT_CODE = "ETH";
    public final static String CRYPT_CODE_PARTS = "Gwei";

    public final static int referralBonus = 10000;//"Gwei"

    public static final float DEF_PLAN_REW = 0.000000021f;
    public static final float PREMIUM_PLAN_REW = 0.000000039f;
    public static final float PLATINUM_PLAN_REW = 0.000000063f;

    public static double getReward(){
        return new Random().nextInt(5000) + 4000;
    }

    public static final String FORMAT_BALANCE = "%.9f";

    public static final float PREMIUM_PRICE = 0.017f;
    public static final float PLATINUM_PRICE = 0.03f;

    public static List<FaqProvider.FaqModel> getExtraFaqs(){
        List<FaqProvider.FaqModel> list = new ArrayList<>();
        list.add(FaqProvider.create(R.string.faq_q101, R.string.faq_a101));
        return list;
    }
}
