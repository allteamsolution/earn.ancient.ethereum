package miner.bitcoin.helper;

import java.util.List;
import java.util.Random;

import miner.bitcoin.provider.FaqProvider;

public class Constants {

    public static final int NOTIFICATION_CODE = 40;

    public static double minPayoutCrypt = 0.25;//LTC

    public static double satoshiToBtc = 0.001 * 0.001 * 0.01;//1 litoshi
    public static double btcToSatoshi = 1000 * 1000 * 100;//1 LTC

    public final static String CRYPT = "Litecoin";
    public final static String CRYPT_CODE = "LTC";
    public final static String CRYPT_CODE_PARTS = "satoshi";

    public final static int referralBonus = 5000;//"satoshi"

    public static final float DEF_PLAN_REW = 0.000000001f;
    public static final float PREMIUM_PLAN_REW = 0.000000002f;
    public static final float PLATINUM_PLAN_REW = 0.000000003f;

    public static double getReward() {
        return new Random().nextInt(2500) + 2000;
    }

    public static final String FORMAT_BALANCE = "%.10f";

    public static final float PREMIUM_PRICE = 0.06f;
    public static final float PLATINUM_PRICE = 0.15f;

    public static List<FaqProvider.FaqModel> getExtraFaqs() {
        return null;
    }
}
