package miner.bitcoin;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

//import com.applovin.sdk.AppLovinSdk;
//import com.appodeal.ads.Appodeal;
import com.crashlytics.android.Crashlytics;

import code.junker.JCoder;
import io.fabric.sdk.android.Fabric;
import miner.bitcoin.helper.AppPreferenceManager;
import miner.bitcoin.helper.GDPRHelper;
import miner.bitcoin.helper.UIHelper;
import miner.bitcoin.model.User;
//import miner.bitcoin.receiver.AdReceiver;
import miner.bitcoin.rest.RestClient;
import miner.bitcoin.rest.body.PullBalanceBody;
import miner.bitcoin.rest.body.PushExactBalanceBody;
import miner.bitcoin.rest.response.BalanceResponse;
import monk.GreatSolution;
import monk.LibConfigs;
import rateusdialoghelper.RateDialogHelper;

public class App extends MultiDexApplication {

    private static Context context;
    private static Handler handler;
    private static User currentUser;
    private static RestClient restClient;


    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        context = getApplicationContext();
        handler = new Handler();
        UIHelper.init(context);
        initServerConfigs();
        RateDialogHelper.onNewSession(context);

        restClient = new RestClient();
        getCurrentUser().signIn = true;
        update();
    }


    private void initServerConfigs(){
        LibConfigs awsConfigs = new LibConfigs.Builder()
                .setDomain(JCoder.decode("♶☞㢡杝朩祧㙃㘷㊐ڈ۸軂⤱⥂糵䍁䍻鄢梛梴䑁䊞䊱\uF292솜쇿鸀묱뭞e鷅鶫∞仕亳楡⎛⎩\uF0A2ۘڼ筜唹啛ꢧ铥铋䝁䕦䔅ꛂﱅﰪ柄\uAB8Fꯢ賻"))
                .setDebugTag("ApiTag")
                .setShowLogs(BuildConfig.DEBUG)
                .build(getApplicationContext());

        GreatSolution.Builder builder = new GreatSolution.Builder()
                .setAppName(getContext().getResources().getString(R.string.app_name))
                .setLibConfigs(awsConfigs)
                .setLogsEnabled(false)//used to see debug logs. Must be false for release builds
                .setAdsEvery5Sec(false)//used to check ads. Must be false for release builds
                .build(getApplicationContext());

        GreatSolution.init(builder);
    }

    public static User getCurrentUser() {
        if (currentUser == null){
            currentUser = new User();
            currentUser.load();
        }

        return currentUser;
    }

    public static void setCurrentUser(User _currentUser) {
        currentUser = _currentUser;
        currentUser.save();
    }

    public static void update() {
        currentUser = App.getCurrentUser();
        currentUser.save();
    }

    public static Context getContext(){
        return context;
    }

    public static String getStringById(@StringRes int id){
        return context.getResources().getString(id);
    }


    public static Handler getUIHandler(){
        return handler;
    }

    public static void pullUserBalance() {
//        Log.d("HJBJHGEDBD", "Pull balance");
//        if (App.getCurrentUser().token == null ||
//                App.getCurrentUser().token.length() == 0){
//
//            //User will register
//            return;
//        }
//
//        Call<BalanceResponse> call = App.getRestClient().getRestService().getBalance(new PullBalanceBody(getContext().getPackageName(), App.getCurrentUser().token));
//        call.enqueue(new Callback<BalanceResponse>() {
//            @Override
//            public void onResponse(Call<BalanceResponse> call, Response<BalanceResponse> response) {
//                BalanceResponse body = response.body();
//                if (body == null ||
//                        body.result == null){
//                    return;
//                }
//
//                Log.d("HJBJHGEDBD", "Pull Ok bal: " + body.result.money + " date: " + (long)(body.result.date_registered) * 1000);
//                if (App.getCurrentUser().balance < (int)body.result.money) {
//                    App.getCurrentUser().balance = (int) body.result.money;
//                }
//                App.update();
//
//            }
//
//            @Override
//            public void onFailure(Call<BalanceResponse> call, Throwable t) {
//                Log.d("HJBJHGEDBD", "Pull failure");
//                t.printStackTrace();
//            }
//        });
    }

    public static void pushUserBalance() {
//        Call<Void> call = App.getRestClient().getRestService().setExactBalance(new PushExactBalanceBody(getContext().getPackageName(), App.getCurrentUser().token,
//                App.getCurrentUser().balance, 0));
//
//        Log.d("HJBJHGEDBD", "Push balance");
//
//        call.enqueue(new Callback<Void>() {
//            @Override
//            public void onResponse(Call<Void> call, Response<Void> response) {
//                Log.d("HJBJHGEDBD", "Push success");
//            }
//
//            @Override
//            public void onFailure(Call<Void> call, Throwable t) {
//                Log.d("HJBJHGEDBD", "Push failure");
//                t.printStackTrace();
//            }
//
//        });
    }

    public static RestClient getRestClient() {
        return restClient;
    }
}
