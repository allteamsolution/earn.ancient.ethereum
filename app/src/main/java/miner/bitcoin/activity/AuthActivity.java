//package miner.bitcoin.activity;
//
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AppCompatActivity;
//import android.widget.FrameLayout;
//
//import butterknife.BindView;
//import butterknife.ButterKnife;
//import miner.bitcoin.R;
//import miner.bitcoin.ctrl.AuthCtrl;
//
//public class AuthActivity extends AppCompatActivity {
//
//    @BindView(R.id.fragment_container)
//    protected FrameLayout fragment_container;
//
//    private AuthCtrl authCtrl;
//
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.auth_activity);
//        ButterKnife.bind(this);
//
//        authCtrl = new AuthCtrl(this);
//        authCtrl.init();
//    }
//
//
//}
