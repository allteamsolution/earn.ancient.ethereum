package miner.bitcoin.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import miner.bitcoin.App;
import miner.bitcoin.BuildConfig;
import miner.bitcoin.R;
import miner.bitcoin.control.InAppNotificationControl;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.MagicDrawable;
import miner.bitcoin.model.User;
import monk.AwsApp;
import monk.helper.AwsAppPreferenceManager;
import monk.helper.loco.LocoMotif;

public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.llOffers)
    protected LinearLayout llOffers;

    @BindView(R.id.llReferal)
    protected LinearLayout llReferal;

    @BindView(R.id.llHome)
    protected LinearLayout llHome;

    @BindView(R.id.llWithdraw)
    protected LinearLayout llWithdraw;


    @BindView(R.id.ivOffers)
    protected ImageView ivOffers;

    @BindView(R.id.ivReferral)
    protected ImageView ivReferral;

    @BindView(R.id.ivHome)
    protected ImageView ivHome;

    @BindView(R.id.ivWithdraw)
    protected ImageView ivWithdraw;

    protected abstract void inflateLayout();

    private static final int TIMEOUT_BIG = 150000;
    private static final int TIMEOUT_SMALL = 10000;

    @BindView(R.id.llNotif)
    protected LinearLayout llNotif;

    private long timeSpendSec;

    private static boolean shownNotif = false;

    private Random random = new Random();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            if (inAppNotificationControl != null) {
                showNotif();
                shownNotif = true;
            }

            delayNotif();
            JunkProvider.f();
        }
    };

    private InAppNotificationControl inAppNotificationControl;

    @Override
    protected void onResume() {
        super.onResume();

        timeSpendSec = System.currentTimeMillis() / 1000;

        int num = 30;
        if (this instanceof MainActivity){
            num = 10;
        }
        if (random.nextInt(num) % num == 0){
            showNotif();
            shownNotif = true;
        }

        App.getUIHandler().removeCallbacks(runnable);
        delayNotif();
    }

    private void delayNotif() {
        int rand = 10000 + random.nextInt(TIMEOUT_BIG);
        if (!shownNotif) {
            rand = 10000 + random.nextInt(TIMEOUT_SMALL);
        }            JunkProvider.f();


        Log.d("TIMESK", "time : " + rand);
        App.getUIHandler().postDelayed(runnable, +rand);
    }

    protected void showNotif() {
        if (AwsApp.getServerConfig().customPropery1.equals("1") || BuildConfig.DEBUG) {
            inAppNotificationControl.show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        inflateLayout();
        ButterKnife.bind(this);

        initBase();
    }

    public static boolean isShownOffers(){
        if (System.currentTimeMillis() - AwsAppPreferenceManager.getInstance().getTimeInstalled() > AwsApp.getServerConfig().getTimeNoOffers()){
            return true;
        }

        return false;
    }

    private void initBase(){
        if (isShownOffers()){
            llOffers.setVisibility(View.VISIBLE);
        } else {
            llOffers.setVisibility(View.GONE);
        }
        ivReferral.setImageDrawable(MagicDrawable.createSelected(
                App.getContext().getResources().getDrawable(R.drawable.ref_small),
                App.getContext().getResources().getColor(R.color.menu_unselected),
                App.getContext().getResources().getColor(R.color.menu_text_color)));

        ivHome.setImageDrawable(MagicDrawable.createSelected(
                App.getContext().getResources().getDrawable(R.drawable.miner_small),
                App.getContext().getResources().getColor(R.color.menu_unselected),
                App.getContext().getResources().getColor(R.color.menu_text_color)));

        ivWithdraw.setImageDrawable(MagicDrawable.createSelected(
                App.getContext().getResources().getDrawable(R.drawable.withdraw_small),
                App.getContext().getResources().getColor(R.color.menu_unselected),
                App.getContext().getResources().getColor(R.color.menu_text_color)));

        ivOffers.setImageDrawable(MagicDrawable.createSelected(
                App.getContext().getResources().getDrawable(R.drawable.offer_small),
                App.getContext().getResources().getColor(R.color.menu_unselected),
                App.getContext().getResources().getColor(R.color.menu_text_color)));

        if (this instanceof MainActivity) {
            llHome.setSelected(true);
        } else if (this instanceof ReferralActivity) {
            llReferal.setSelected(true);
        } else if (this instanceof PayoutActivity){
            llWithdraw.setSelected(true);
        } else if (this instanceof OffersActivity){
            llOffers.setSelected(true);
        }

        inAppNotificationControl = new InAppNotificationControl(llNotif);

    }

    @OnClick(R.id.llHome)
    public void onHomeClicked(){
        if (this instanceof MainActivity){
            return;
        }

        Intent intent = new Intent(this, MainActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.llReferal)
    public void ReferralActivityClicked(){
        if (this instanceof ReferralActivity){
            return;
        }

        Intent intent = new Intent(this, ReferralActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.llWithdraw)
    public void PayoutActivityClicked(){
//        InterstitialAdLogicControl.showWebView();

        if (this instanceof PayoutActivity){
            return;
        }

        Intent intent = new Intent(this, PayoutActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.llOffers)
    public void offersClicked(){
        if (this instanceof OffersActivity){
            return;
        }

        LocoMotif controller = LocoMotif.create(LocoMotif.Builder.create()
                .setCurrencyName("ethereum")
                .setWithOffers(true)
                .setColorAcent(Color.parseColor("#492983"))
                .setColorAcentPressed(Color.parseColor("#492983"))
                .setRewardMultiplier(0.003f)
                .setFormat(3)
                .setLocoRewardCallback(new LocoMotif.LocoRewardCallback() {
                    @Override
                    public float getMyBalance() {
                        double balance = App.getCurrentUser().balance;
                        return (float) balance ;
                    }

                    @Override
                    public void onTotalReward(float totalReward, float delta) {
                        User user = App.getCurrentUser();
                        user.balance += (delta * 1.00f);
                        App.setCurrentUser(user);
                    }
                }));

        controller.open(this);

//        Intent intent = new Intent(this, OffersActivity.class);
//        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
//        startActivity(intent);
//        finish();
    }

    @Override
    public void onBackPressed() {
        if (this instanceof MainActivity){
            //TODO add ad here
            super.onBackPressed();
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            overridePendingTransition(R.anim.tab_activity_transition_in, R.anim.tab_activity_transition_out);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.tab_activity_transition_in, R.anim.tab_activity_transition_out);
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.getUIHandler().removeCallbacks(runnable);

        timeSpendSec = System.currentTimeMillis() / 1000 - timeSpendSec;
//
//        Answers.getInstance().logCustom(new CustomEvent(getClass().getSimpleName())
//                .putCustomAttribute(AnalHelper.TIME_ON_SCREEN, timeSpendSec));
    }

}

