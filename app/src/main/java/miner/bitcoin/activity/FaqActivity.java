package miner.bitcoin.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.provider.FaqProvider;

public class FaqActivity extends AppCompatActivity {

    public static final String MARKET_URL_PREFIX = "market://details?id=";
    public static final String PACK = "clientapp.swiftcom.org";

    @BindView(R.id.abBack)
    public ImageView abBack;

    @BindView(R.id.llContent)
    public LinearLayout llContent;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.faq_activity);
        ButterKnife.bind(this);

        init();
    }

    private void init(){
        Utility.setDrawableColor(abBack, App.getContext().getResources().getColor(R.color.white));

        List<FaqProvider.FaqModel> models = FaqProvider.provide();
        for (FaqProvider.FaqModel model : models){
            View item = LayoutInflater.from(this).inflate(R.layout.faq_item, null);
            FaqHolder settingView = new FaqHolder(item);

            settingView.bind(model);
            llContent.addView(settingView.parent);
        }
    }

    protected static class FaqHolder{

        @BindView(R.id.tvTitle)
        public TextView tvTitle;

        @BindView(R.id.tvDescr)
        public TextView tvDescr;

        protected View parent;

        public FaqHolder(View parent){
            this.parent = parent;
            ButterKnife.bind(this, parent);
        }

        public void bind(final FaqProvider.FaqModel settingModel){
            tvTitle.setText(settingModel.question);
            tvDescr.setText(settingModel.answer);
        }
    }

    @OnClick(R.id.abBack)
    public void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.tab_activity_transition_in, R.anim.tab_activity_transition_out);
    }

    @OnClick(R.id.btnInstall)
    public void onBtnInstallClick() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL_PREFIX + PACK)));
    }

}
