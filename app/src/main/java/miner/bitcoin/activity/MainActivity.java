package miner.bitcoin.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.appodeal.ads.Appodeal;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.adapter.MainAdapter;
import miner.bitcoin.event.MineStartedEvent;
import miner.bitcoin.event.MineStoppedEvent;
import miner.bitcoin.event.MinedEvent;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.FinalConstants;
import miner.bitcoin.helper.MyRateDialogHelper;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.model.User;
import miner.bitcoin.receiver.NetworkStateReceiver;
import miner.bitcoin.service.ForegroundService;
import monk.AwsApp;
import monk.helper.AwsApiHelper;
import monk.helper.GDPRHelper;
import monk.helper.GreatNativeHelper;
import monk.helper.VersionUpdater;
import monk.helper.finalmigration.FinalMigrationHelper;
import monk.helper.migration.CraneMigrationHelper;
import monk.model.CustomAd;

public class MainActivity extends BaseActivity implements MainAdapter.MainAdapterCallback, NetworkStateReceiver.NetworkStateReceiverListener {

    private static final int PLAN = 324;

    private static Random rand = new Random();
    private static int wasindex = -1;

    @BindView(R.id.tvMiningLeftTime)
    protected TextView tvMiningLeftTime;

    @BindView(R.id.tvBalance)
    protected TextView tvBalance;

    @BindView(R.id.rvItems)
    public RecyclerView rvItems;

    @BindView(R.id.llNoInet)
    protected ViewGroup llNoInet;

    @BindView(R.id.adView)
    public FrameLayout adView;


    @BindView(R.id.ivStartStop)
    public ImageView ivStartStop;

    private boolean mBound = false;
    private ForegroundService mService;

    private MainAdapter mainAdapter;

    private Drawable play, pause;

    private NetworkStateReceiver networkStateReceiver;

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);
        init();

        VersionUpdater.check(this, miner.bitcoin.BuildConfig.VERSION_NAME, getString(R.string.app_name));

        GDPRHelper.initConsentAndPrivacyDialog(this, getResources().getColor(R.color.colorAccent), true);

        CraneMigrationHelper.builder(this)
                .withBonuses("10", "cents")
                .withRewardCallback(new CraneMigrationHelper.MigrationRewardCallback() {
                    @Override
                    public void onReward() {
                        App.getCurrentUser().increaseBalance(10*.01);
                        updateBalance();
                    }
                }).show();
        FinalMigrationHelper.builder(this).show();
//        initAdReward();
    }

    /*private void initAdReward() {
        int cooldownReward = 300;
        if (App.getCurrentUser().lastWatchingAdd < Utility.getTime() - cooldownReward) {

        }
    }*/


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MinedEvent event) {
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStartedEvent event) {
        initStartBtn(true);
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStoppedEvent event) {
        initStartBtn(false);
        updateBalance();
    }

    @Override
    public void tvUpgradeCLicked(){
        Intent intent = new Intent(this, PlanActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivityForResult(intent, PLAN);
        //go to plans
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
        // Bind to LocalService
        Intent intent = new Intent(this, ForegroundService.class);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);

        if (mService != null) {
            initStartBtn(mService.isRunning());
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        unbindService(mConnection);
        mBound = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode){
            case PLAN:
                if (resultCode == RESULT_OK){
                    updatePlan();
                }
                break;

        }
    }

    private void initStartBtn(boolean isServiceRunning) {
        if (isServiceRunning) {
            tvMiningLeftTime.setText(R.string.miner_is_running);
            ivStartStop.setImageDrawable(pause);
        } else {
            tvMiningLeftTime.setText(R.string.miner_is_stopped);
            ivStartStop.setImageDrawable(play);
        }

        updateBalance();
    }

    private void updateBalance(){
        float balance = App.getCurrentUser().balance;
        tvBalance.setText(Utility.formatBalance(balance) + " " + Constants.CRYPT_CODE);
    }

    private void updatePlan(){
        mainAdapter.updatePlan();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkStateReceiver);
//        if (mainAdapter.rewardViewHolder != null)
//            mainAdapter.rewardViewHolder.clearTimer();
    }

    private void init() {
        initRecyclerView();


        AwsApiHelper.checkUpdate();

        if (!App.getCurrentUser().isDemoPremiumUsed) {
            MyRateDialogHelper.show(this);
        }

        play = getResources().getDrawable(R.drawable.baseline_play_arrow_white_36);
        pause = getResources().getDrawable(R.drawable.baseline_pause_white_36);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        setNoInetWidgetVisible(!Utility.hasInternet(this));
    }

    private void initRecyclerView(){
        mainAdapter = new MainAdapter(this);
        mainAdapter.setItems(getMainAdapterItems());

//        if (AwsApp.getServerConfig().showNativeShitAds()) {
//            GreatNativeHelper.addStartApp(this, new solution.great.lib.helper.GreatNativeHelper.StartappNativeLoadedCallback() {
//                @Override
//                public void onLoaded(NativeAdDetails nativeAdDetails) {
//                    mainAdapter.addItem(MainAdapter.MainModel.createStartapp(nativeAdDetails));
//                }
//
//                @Override
//                public void onFailure() {
//
//                }
//            });
//        }

        mainAdapter.addItem(MainAdapter.MainModel.createAdMobNative());

//        NativeAdHelper.addAppodealAd(mainAdapter);

        LinearLayoutManager quickLinkLayoutManager = new LinearLayoutManager(rvItems.getContext(), LinearLayoutManager.VERTICAL, false);
        rvItems.setLayoutManager(quickLinkLayoutManager);
        rvItems.setHasFixedSize(true);
        rvItems.setAdapter(mainAdapter);
    }

    public static CustomAd getRandomCustomAd(){

        List<CustomAd> list = AwsApp.getCustomAds(CustomAd.CATEGORY_CRYPTO);

        if (list.size() <= 1){
            list = AwsApp.getCustomAds();
        }
        //fix for too small amount for crypto aps for ads
        else if (list.size() <= 3){
            if (new Random().nextInt(2) == 0) {
                list = AwsApp.getCustomAds();
            }
        }

        if (list.size() > 0) {
            int size = list.size();
            int index = rand.nextInt(size);
            if (list.get(index).packageName.equalsIgnoreCase(App.getContext().getPackageName())){
                if (index > 0) {
                    --index;
                } else {
                    index = size - 1;
                }
            }

            if (index == wasindex){
                if (index > 0) {
                    --index;
                } else {
                    index = size - 1;
                }
            }

            wasindex = index;

            return list.get(index);
        }

        return null;
    }





    private List<MainAdapter.MainModel> getMainAdapterItems(){
        List<MainAdapter.MainModel> mPayModels = new ArrayList<>();
        mPayModels.add(MainAdapter.MainModel.createPlan());

//        if (isShownOffers())
//            mPayModels.add(MainAdapter.MainModel.createRewardAd());
        if (isShownOffers())
            rvItems.setVisibility(View.VISIBLE);


        CustomAd customAd = getRandomCustomAd();
        if (customAd != null) {
            mPayModels.add(MainAdapter.MainModel.createCustomAd(customAd));
        }

        CustomAd customAd1 = getRandomCustomAd();
        if (customAd1 != null) {
            mPayModels.add(MainAdapter.MainModel.createCustomAd(customAd1));
        }

        return mPayModels;
    }

    @Override
    protected void onPause() {
        super.onPause();
        App.pushUserBalance();
    }

    @Override
    protected void onResume() {
        super.onResume();
        App.pullUserBalance();

//        if (!App.getCurrentUser().privacyAgreed){
//            PrivacyDialog.show(this, new PrivacyDialog.PrivacyDialogCallback() {
//                @Override
//                public void onOpenPrivacy(String url) {
//                    Intent i = new Intent(Intent.ACTION_VIEW);
//                    i.setData(Uri.parse(url));
//                    startActivity(i);
//                }
//            });
//        }
    }

    @OnClick(R.id.abShare)
    public void onShareClicked(){
        Utility.share(this, true);
    }

    @OnClick(R.id.abSettings)
    public void onSettingsClicked(){
        Intent intent = new Intent(this, SettingsActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivity(intent);
    }

    private ServiceConnection mConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName className,
                                       IBinder service) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            ForegroundService.LocalBinder binder = (ForegroundService.LocalBinder) service;
            mService = binder.getService();
            mBound = true;

            initStartBtn(mService.isRunning());
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mBound = false;
        }
    };

    @Override
    public void onCustomAdClicked(String packagename) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FinalConstants.MARKET_URL_PREFIX + packagename)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(FinalConstants.MARKET_BROWSER_URL_PREFIX + packagename)));
        }
    }

//    @Override
//    public void onRewardAdClicked() {
//       // if (App.getCurrentUser().lastWatchingAdd + Constants.TIME_AD_REWARDS_SEC  < Utility.getTime())
//        //    AdmobRewardHelper.show(this);
//    }

    @OnClick(R.id.ivStartStop)
    public void onivStartStopClicked(){


        if (mService != null){
            if (mService.isRunning()){
                Intent service = ForegroundService.createIntent(ForegroundService.ACTION_STOP_MINING);
                App.getContext().startService(service);
            } else {
                Log.d("JJJJJJJJ", "save time : " + Utility.getTime());
                App.getCurrentUser().timeLastRestart = Utility.getTime();
                App.setCurrentUser(App.getCurrentUser());

                Intent service = ForegroundService.createIntent(ForegroundService.ACTION_START_MINING);
                App.getContext().startService(service);
            }
        } else {
            Toast.makeText(this, "Please, try again later, internal error", Toast.LENGTH_LONG).show();
        }
    }

    private void setNoInetWidgetVisible(boolean visible){
        if (visible){
            llNoInet.setVisibility(View.VISIBLE);
        } else {
            llNoInet.setVisibility(View.GONE);
        }
    }

    @Override
    public void networkAvailable() {
        setNoInetWidgetVisible(false);
    }

    @Override
    public void networkUnavailable() {
        setNoInetWidgetVisible(true);
    }
}
