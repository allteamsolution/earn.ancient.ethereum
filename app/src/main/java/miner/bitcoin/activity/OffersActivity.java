package miner.bitcoin.activity;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.adapter.OfferAdapter;
import miner.bitcoin.event.MineStartedEvent;
import miner.bitcoin.event.MineStoppedEvent;
import miner.bitcoin.event.MinedEvent;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.model.User;
import miner.bitcoin.receiver.NetworkStateReceiver;
import miner.bitcoin.view.AppRecyclerView;
import monk.model.Offer;
import monk.model.modelhelper.OffersHelper;

public class OffersActivity extends BaseActivity implements NetworkStateReceiver.NetworkStateReceiverListener, OfferAdapter.PayAdapterCallback {

    private static final long OFFER_TIMER_NONE = -1;

    @BindView(R.id.tvBalance)
    protected TextView tvBalance;

    @BindView(R.id.llNoInet)
    protected ViewGroup llNoInet;

    @BindView(R.id.rvItems)
    public AppRecyclerView rvItems;

    @BindView(R.id.emptyView)
    protected LinearLayout emptyView;

    @BindView(R.id.adView)
    public FrameLayout adView;


    @BindView(R.id.abBack)
    protected ImageView abBack;

    private NetworkStateReceiver networkStateReceiver;

    private long onOfferTimerStarted = OFFER_TIMER_NONE;
    private int secondsMustBeSpend = 5;

    private OfferAdapter offerAdapter;

    private boolean isPlusBalanceDisplayed = false;

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.offrers_activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        networkStateReceiver = new NetworkStateReceiver();
        networkStateReceiver.addListener(this);
        this.registerReceiver(networkStateReceiver, new IntentFilter(android.net.ConnectivityManager.CONNECTIVITY_ACTION));

        init();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(networkStateReceiver);
        App.getUIHandler().removeCallbacks(runnable);
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MinedEvent event) {
        if (!isPlusBalanceDisplayed) {
            updateBalance();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStartedEvent event) {
        if (!isPlusBalanceDisplayed) {
            updateBalance();
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStoppedEvent event) {
        if (!isPlusBalanceDisplayed) {
            updateBalance();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();

        if (onOfferTimerStarted != OFFER_TIMER_NONE){
            if (System.currentTimeMillis() - onOfferTimerStarted > secondsMustBeSpend * 1000){
                double rewardInSatoshi = Constants.getReward();
                double reward = rewardInSatoshi * Constants.satoshiToBtc;
                User user = App.getCurrentUser();
                float bal = user.balance;

                user.increaseBalance(reward);
                App.setCurrentUser(user);

                updateBalanceOffer(bal, (int) rewardInSatoshi);
            }

            onOfferTimerStarted = OFFER_TIMER_NONE;
        }
    }

    private void init(){
        Utility.setDrawableColor(abBack, getResources().getColor(R.color.white));

        updateBalance();

        offerAdapter = new OfferAdapter(this);

        LinearLayoutManager quickLinkLayoutManager = new LinearLayoutManager(rvItems.getContext(), LinearLayoutManager.VERTICAL, false);
        rvItems.setLayoutManager(quickLinkLayoutManager);
        rvItems.setHasFixedSize(true);
        rvItems.setAdapter(offerAdapter);

        offerAdapter.networkAvailable(Utility.hasInternet(this));
        setNoInetWidgetVisible(!Utility.hasInternet(this));

        rvItems.setEmptyView(emptyView);

//        AdmobBanerHelper.init(adView);
    }

    private void setNoInetWidgetVisible(boolean visible){
        if (visible){
            llNoInet.setVisibility(View.VISIBLE);
        } else {
            llNoInet.setVisibility(View.GONE);
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            updateBalance();
        }
    };

    private void updateBalance(){
        float balance = App.getCurrentUser().balance;
        tvBalance.setText(Utility.formatBalance(balance) + " " + Constants.CRYPT_CODE);
    }

    private void updateBalanceOffer(float balance, int reward){
        tvBalance.setText(Utility.formatBalance(balance) + " + " + reward + " " + Constants.CRYPT_CODE_PARTS);

        isPlusBalanceDisplayed = true;
        App.getUIHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isPlusBalanceDisplayed = false;
                updateBalance();
            }
        }, 3000);
    }

    @Override
    public void networkAvailable() {
        offerAdapter.networkAvailable(true);
        setNoInetWidgetVisible(false);
    }

    @Override
    public void networkUnavailable() {
        offerAdapter.networkAvailable(false);
        setNoInetWidgetVisible(true);
    }

    @OnClick(R.id.abBack)
    public void abBackClick(){
        onBackPressed();
    }

    @Override
    public void onOfferClicked(Offer offer) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(offer.url));
        startActivity(i);

        try {
            offer.setIsClicked(true);
            OffersHelper.update(offer);
        } catch (Throwable t){}

        this.secondsMustBeSpend = 5;
        onOfferTimerStarted = System.currentTimeMillis();
    }
}

