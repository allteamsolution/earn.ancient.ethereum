package miner.bitcoin.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.event.MineStartedEvent;
import miner.bitcoin.event.MineStoppedEvent;
import miner.bitcoin.event.MinedEvent;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.RefCodeGenerator;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.model.User;

public class PayoutActivity extends BaseActivity {

    @BindView(R.id.etWallet)
    protected EditText etWallet;

    @BindView(R.id.ivIcon)
    protected ImageView ivIcon;

    @BindView(R.id.llWallet)
    public LinearLayout llWallet;

    @BindView(R.id.llWithdrawals)
    public LinearLayout llWithdrawals;


    @BindView(R.id.ivWarning)
    protected ImageView ivWarning;

    @BindView(R.id.tvBalance)
    protected TextView tvBalance;

    @BindView(R.id.tvExplanation)
    protected TextView tvExplanation;

    @BindView(R.id.btnGetBtc)
    protected Button btnGetBtc;

//    @BindView(R.id.tvInfo)
//    protected TextView tvInfo;

    @BindView(R.id.llExplanations)
    protected LinearLayout llExplanations;

    @BindView(R.id.adView)
    public FrameLayout adView;


    @Override
    protected void inflateLayout() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.payout_activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void init(){
        Utility.setDrawableColor(ivIcon, App.getContext().getResources().getColor(R.color.white));
        Utility.setDrawableColor(ivWarning, App.getContext().getResources().getColor(R.color.white));

        User user = App.getCurrentUser();
        etWallet.setText(user.walletAddress);

        updateBalance();

        paintExplanation();
        float balance = user.balance;

        if (balance < Constants.minPayoutCrypt){
            btnGetBtc.setEnabled(false);
            llExplanations.setVisibility(View.VISIBLE);
        } else {
            btnGetBtc.setEnabled(true);
            llExplanations.setVisibility(View.GONE);
        }

//        AdmobBanerHelper.init(adView);

        addPendingWithdrawal();
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MinedEvent event) {
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStartedEvent event) {
        updateBalance();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MineStoppedEvent event) {
        updateBalance();
    }

    private void updateBalance(){
        User user = App.getCurrentUser();
        float balance = user.balance;
        tvBalance.setText(Utility.formatBalance(balance) + " " + Constants.CRYPT_CODE);
    }

    @Override
    protected void onPause() {
        super.onPause();

        User user = App.getCurrentUser();
        user.walletAddress = etWallet.getText().toString();
        App.setCurrentUser(user);
    }

//    private void paintPayoutText(){
//        String pre = getResources().getString(R.string.pay_info_1);
//        String key = getResources().getString(R.string.pay_info_2);
//        String suf = getResources().getString(R.string.pay_info_3);
//        String suf2 = getResources().getString(R.string.pay_info_4);
//
//        ForegroundColorSpan spanWhite1 = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.white));
//        ForegroundColorSpan spanWhite = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.white));
//        ForegroundColorSpan spanFab = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.fabcolor));
//        ForegroundColorSpan spanFab1 = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.fabcolor));
//
//        int size = pre.length();
//        pre += " " + key;
//        int size2 = pre.length();
//        pre += " " + suf;
//        int size3 = pre.length();
//        pre += " " + suf2;
//
//        Spannable spannable = new SpannableString(pre);
//        spannable.setSpan(spanWhite1, 0, size, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        spannable.setSpan(spanFab1, size, size2, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        spannable.setSpan(spanWhite, size2, size3, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        spannable.setSpan(spanFab, size3, pre.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//
//        tvInfo.setText(spannable);
//    }

    private void paintExplanation(){
        String str0 = App.getContext().getResources().getString(R.string.why_cannot_payout);
        String str1 = App.getContext().getResources().getString(R.string.why_cannot_payout_suffix, Constants.minPayoutCrypt);

        ForegroundColorSpan spanWhite = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.white));
        ForegroundColorSpan spanFab = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.fabcolor));

        int size = str0.length();
        str0 += " " + str1;
        Spannable spannable = new SpannableString(str0);
        spannable.setSpan(spanWhite, 0, size, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannable.setSpan(spanFab, size, str0.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        tvExplanation.setText(spannable);
    }

    @OnClick(R.id.btnGetBtc)
    public void clickPayout(){
        if (App.getCurrentUser().balance >= Constants.minPayoutCrypt) {
            if (etWallet.getText() == null || etWallet.getText().length() == 0) {
                Toast.makeText(this, "Enter wallet address", Toast.LENGTH_LONG).show();
                return;
            }
            Toast.makeText(this, R.string.money_send, Toast.LENGTH_LONG).show();

            User user = App.getCurrentUser();
            user.balanceUserTryToWithdraw = user.balance;
            user.balanceUserTryToWithdrawTime = Utility.getTime();
            user.balance = 0.0f;
            App.setCurrentUser(user);

            addPendingWithdrawal();
            updateBalance();
        }
    }

    private static final long DAY = 1000 * 60 * 60 * 24;

    private void addPendingWithdrawal(){
        User user = App.getCurrentUser();

        if (user.balanceUserTryToWithdraw > 0){
            TextView textView = new TextView(this);
            textView.setTextColor(Color.WHITE);

            long timeDiff = System.currentTimeMillis() - user.balanceUserTryToWithdrawTime;
            if (timeDiff < 5 * DAY){
                textView.setText(Utility.getDate(user.balanceUserTryToWithdrawTime) + "  Pending: " + Utility.formatBalance(user.balanceUserTryToWithdraw));
            } else if (timeDiff < 10 * DAY){
                textView.setText(Utility.getDate(user.balanceUserTryToWithdrawTime) + "  Approved: " + Utility.formatBalance(user.balanceUserTryToWithdraw));
            } else if (timeDiff < 15 * DAY){
                textView.setText(Utility.getDate(user.balanceUserTryToWithdrawTime) + "  In Processing: " + Utility.formatBalance(user.balanceUserTryToWithdraw));
            } else {
                textView.setText(Utility.getDate(user.balanceUserTryToWithdrawTime) + "  Sent: " + Utility.formatBalance(user.balanceUserTryToWithdraw) + "\n" +
                "Trans id: " + RefCodeGenerator.generateTxs() + "..");
            }

            llWithdrawals.addView(textView);
        } else {
            TextView textView = new TextView(this);
            textView.setTextColor(Color.WHITE);
            textView.setText(R.string.payout_will_be_here);
            llWithdrawals.addView(textView);
        }
    }


}
