package miner.bitcoin.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.dialog.BoughtItemDialog;
import miner.bitcoin.dialog.RateUsHelper;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.model.User;
import monk.helper.KeywordHelper;

public class PlanActivity extends AppCompatActivity {

    @BindView(R.id.ivCheckFreePlan)
    public ImageView ivCheckFreePlan;

    @BindView(R.id.ivCheckFreePlan2)
    public ImageView ivCheckFreePlan2;

    @BindView(R.id.ivCheckFreePlan3)
    public ImageView ivCheckFreePlan3;

    @BindView(R.id.ivCheckFreePlanMark)
    public ImageView ivCheckFreePlanMark;

    @BindView(R.id.tvCurPlanMarkDescr)
    public TextView tvCurPlanMarkDescr;

    @BindView(R.id.abBack)
    public ImageView abBack;

    @BindView(R.id.llPlanMark)
    protected ViewGroup llPlanMark;

    private boolean onRateGoneToGP = false;
    private long gpTime;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.plan_activity);
        ButterKnife.bind(this);

        Utility.setDrawableColor(abBack, App.getContext().getResources().getColor(R.color.white));
        Utility.setDrawableColor(ivCheckFreePlan, App.getContext().getResources().getColor(R.color.colorPrimary));
        Utility.setDrawableColor(ivCheckFreePlan2, App.getContext().getResources().getColor(R.color.colorPrimary));
        Utility.setDrawableColor(ivCheckFreePlan3, App.getContext().getResources().getColor(R.color.colorPrimary));
        Utility.setDrawableColor(ivCheckFreePlanMark, App.getContext().getResources().getColor(R.color.colorPrimary));

        initBtns();
    }

    private void initBtns() {
        ivCheckFreePlan.setVisibility(View.GONE);
        ivCheckFreePlan2.setVisibility(View.GONE);
        ivCheckFreePlan3.setVisibility(View.GONE);
        ivCheckFreePlanMark.setVisibility(View.GONE);

        switch (App.getCurrentUser().plan) {
            case User.PLAN_DEF:
                ivCheckFreePlan.setVisibility(View.VISIBLE);
                break;
            case User.PLAN_PREMIUM:
                ivCheckFreePlan2.setVisibility(View.VISIBLE);
                break;
            case User.PLAN_PLATINUM:
                ivCheckFreePlan3.setVisibility(View.VISIBLE);
                break;
            case User.PLAN_PREMIUM_DEMO:
                ivCheckFreePlanMark.setVisibility(View.VISIBLE);
                break;
        }

        if (!BaseActivity.isShownOffers()){
            llPlanMark.setVisibility(View.GONE);
        } else {
            if (!App.getCurrentUser().isDemoPremiumUsed || App.getCurrentUser().isDemoPremiumActive()){
                llPlanMark.setVisibility(View.VISIBLE);
            } else {
                llPlanMark.setVisibility(View.GONE);
            }
        }

        if (KeywordHelper.getKeyword() != null) {
            tvCurPlanMarkDescr.setText(tvCurPlanMarkDescr.getText().toString() + "\n" + KeywordHelper.getKeyword());
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (onRateGoneToGP && System.currentTimeMillis() - gpTime > 10 * 1000){
            App.getUIHandler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    onRateGoneToGP = false;
                    App.getCurrentUser().plan = User.PLAN_PREMIUM_DEMO;
                    App.getCurrentUser().timeDemoPremiumStart = System.currentTimeMillis();
                    App.getCurrentUser().isDemoPremiumUsed = true;
                    App.setCurrentUser(App.getCurrentUser());

                    initBtns();

                    BoughtItemDialog.show(PlanActivity.this);
                    setResult(RESULT_OK);
                }
            }, 500);

            onRateGoneToGP = false;
        }
    }

    @OnClick(R.id.btnBuyPremiumMark)
    protected void premiumDemo() {
        if (!App.getCurrentUser().isDemoPremiumUsed) {
            goToGP();
        }
    }

    private void goToGP(){
        RateUsHelper.show(this, new RateUsHelper.RateUsHelperCallback() {
            @Override
            public void onGoneToGooglePlay() {
                onRateGoneToGP = true;
                gpTime = System.currentTimeMillis();

                Uri uri = Uri.parse("market://details?id=" + App.getContext().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + App.getContext().getPackageName())));
                }
            }
        });
    }

    @OnClick(R.id.btnBuyPremium)
    public void onClickPremPlan() {
        if (App.getCurrentUser().balance >= Constants.PREMIUM_PRICE) {
            App.getCurrentUser().balance -= Constants.PREMIUM_PRICE;
            App.getCurrentUser().plan = User.PLAN_PREMIUM;
            App.setCurrentUser(App.getCurrentUser());

            initBtns();

            BoughtItemDialog.show(this);
            setResult(RESULT_OK);
        } else {
            Toast.makeText(this, R.string.no_money, Toast.LENGTH_LONG).show();
        }

    }

    @OnClick(R.id.btnBuyPlatinum)
    public void onClickPlatPlan() {
        if (App.getCurrentUser().balance >= Constants.PLATINUM_PRICE) {
            App.getCurrentUser().balance -= Constants.PLATINUM_PRICE;
            App.getCurrentUser().plan = User.PLAN_PLATINUM;
            App.setCurrentUser(App.getCurrentUser());

            initBtns();

            BoughtItemDialog.show(this);
            setResult(RESULT_OK);
        } else {
            Toast.makeText(this, R.string.no_money, Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.abBack)
    public void onabBackClick() {
        onBackPressed();
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.tab_activity_transition_in, R.anim.tab_activity_transition_out);
    }
}
