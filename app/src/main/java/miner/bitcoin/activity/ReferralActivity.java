package miner.bitcoin.activity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.BuildConfig;
import miner.bitcoin.R;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.model.User;

import static miner.bitcoin.helper.RefCodeGenerator.REF_CODE_LEN;

public class ReferralActivity extends BaseActivity {

    @BindView(R.id.adView)
    public FrameLayout adView;


    @BindView(R.id.abBack)
    public ImageView abBack;

    @BindView(R.id.ivShare)
    protected ImageView ivShare;

    @BindView(R.id.ivCopy)
    protected ImageView ivCopy;

    @BindView(R.id.tvRefCode)
    protected TextView tvRefCode;

    @BindView(R.id.etRefCode)
    protected EditText etRefCode;

    @Override
    protected void inflateLayout() {
        setContentView(R.layout.referral_activity);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ButterKnife.bind(this);

        init();
    }

    private void init() {
        initAdmob();

        if (BuildConfig.FLAVOR.equalsIgnoreCase("ethereum")){
            Utility.setDrawableColor(ivShare, App.getContext().getResources().getColor(R.color.white));
            Utility.setDrawableColor(ivCopy, App.getContext().getResources().getColor(R.color.white));
            abBack.setVisibility(View.GONE);
        } else {
            Utility.setDrawableColor(ivShare, App.getContext().getResources().getColor(R.color.primary_text));
            Utility.setDrawableColor(ivCopy, App.getContext().getResources().getColor(R.color.primary_text));
        }

        Utility.setDrawableColor(abBack, App.getContext().getResources().getColor(R.color.white));

        tvRefCode.setText(App.getCurrentUser().refCode);

        if (App.getCurrentUser().refCodeApplied) {
            etRefCode.setText(App.getCurrentUser().myReferrer);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initAdmob() {
    }

    @OnClick(R.id.ivCopy)
    public void onCopyRefCode() {
        String ref = tvRefCode.getText().toString();

        ClipboardManager manager = (ClipboardManager) this.getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData data = ClipData.newPlainText(null, ref.trim());
        manager.setPrimaryClip(data);
        Toast.makeText(this, R.string.toast_copy_successful, Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.ivShare)
    public void shareClicked() {
        Utility.share(this, true);
    }

    @OnClick(R.id.abBack)
    public void onBackClicked() {
        close();
    }

    @Override
    public void onBackPressed() {
        close();
    }

    @OnClick(R.id.tvApply)
    public void tvApplyClick(){
        User user = App.getCurrentUser();

        if (!user.refCodeApplied && etRefCode.getText() != null && etRefCode.getText().toString().length() == REF_CODE_LEN) {
            user.refCodeApplied = true;
            user.myReferrer = etRefCode.getText().toString();
            user.increaseBalance(Constants.referralBonus * 0.01);

            Toast.makeText(this, R.string.ref_code_successful, Toast.LENGTH_SHORT).show();

            App.setCurrentUser(user);
        }

    }

    private void close() {
        super.onBackPressed();
    }

}

