package miner.bitcoin.activity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.BuildConfig;
import miner.bitcoin.R;
import miner.bitcoin.helper.AppPreferenceManager;
import miner.bitcoin.helper.GDPRHelper;
import miner.bitcoin.helper.Utility;
import monk.AwsApp;
import monk.helper.GDPRWidget;

public class SettingsActivity extends AppCompatActivity {

    @BindView(R.id.abBack)
    public ImageView abBack;

    @BindView(R.id.ivIcon)
    public ImageView ivIcon;

    @BindView(R.id.ivIcon1)
    public ImageView ivIcon1;

    @BindView(R.id.ivIcon3)
    public ImageView ivIcon3;

    @BindView(R.id.ivIcon4)
    public ImageView ivIcon4;

    @BindView(R.id.ivIcon31)
    public ImageView ivIcon31;

    @BindView(R.id.tvUser)
    public TextView tvUser;

    @BindView(R.id.faq)
    public RelativeLayout faq;

    @BindView(R.id.upgradePlan)
    public RelativeLayout upgradePlan;

    @BindView(R.id.parent)
    public LinearLayout parent;

    @BindView(R.id.tvTitle1)
    public TextView tvTitle1;

    @BindView(R.id.tvTitle)
    public TextView tvTitle;

    @BindView(R.id.tvTitle31)
    public TextView tvTitle31;

    @BindView(R.id.tvTitle3)
    public TextView tvTitle3;

    @BindView(R.id.tvTitle4)
    public TextView tvTitle4;

    @BindView(R.id.tvSite)
    public TextView tvSite;

    @BindView(R.id.flGdpr)
    protected FrameLayout flGdpr;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);
        ButterKnife.bind(this);

        init();
    }

    private void init(){
        if (BuildConfig.FLAVOR.equalsIgnoreCase("ethereum")) {
            parent.setBackgroundColor(Color.BLACK);
            tvTitle1.setTextColor(Color.WHITE);
            tvTitle.setTextColor(Color.WHITE);
            tvTitle31.setTextColor(Color.WHITE);
            tvTitle3.setTextColor(Color.WHITE);
            tvTitle4.setTextColor(Color.WHITE);
        }

        Utility.setDrawableColor(abBack, App.getContext().getResources().getColor(R.color.white));
        Utility.setDrawableColor(ivIcon, App.getContext().getResources().getColor(R.color.secondary_text));
        Utility.setDrawableColor(ivIcon1, App.getContext().getResources().getColor(R.color.secondary_text));
        Utility.setDrawableColor(ivIcon3, App.getContext().getResources().getColor(R.color.secondary_text));
        Utility.setDrawableColor(ivIcon4, App.getContext().getResources().getColor(R.color.secondary_text));
        Utility.setDrawableColor(ivIcon31, App.getContext().getResources().getColor(R.color.secondary_text));

        GDPRWidget.create(flGdpr).build();

        tvUser.setText("Miner id: " + App.getCurrentUser().userId);

        String url = AwsApp.getServerConfig().getSiteUrl();
        tvSite.setText(url);
    }

    @OnClick(R.id.abBack)
    public void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.faq)
    public void onfaqClicked() {
        Intent intent = new Intent(this, FaqActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivity(intent);
    }

    @OnClick(R.id.upgradePlan)
    public void onupgradePlanClicked() {
        Intent intent = new Intent(this, PlanActivity.class);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        startActivity(intent);
    }

    @OnClick(R.id.privacy)
    public void privacyPolicyClicked() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(AwsApp.getServerConfig().privacyPolicyUrl));
            startActivity(i);
        } catch (Throwable t){

        }
    }

    @OnClick(R.id.site)
    public void siteClicked() {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(AwsApp.getServerConfig().siteUrl));
            startActivity(i);
        } catch (Throwable t){

        }
    }

    @OnClick(R.id.support)
    public void supportClicked() {
        Intent intent = new Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:" + getResources().getString(R.string.support_email)));

        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{getResources().getString(R.string.support_email)});
        intent.putExtra(Intent.EXTRA_SUBJECT, "Request form user #" + App.getCurrentUser().userId);

        startActivity(Intent.createChooser(intent, getResources().getString(R.string.send_via)));
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.tab_activity_transition_in, R.anim.tab_activity_transition_out);
    }
}
