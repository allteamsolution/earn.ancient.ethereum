package miner.bitcoin.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.TimeConverter;
import miner.bitcoin.model.User;
import monk.AwsApp;
import monk.model.CustomAd;

public class MainAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int TYPE_PLAN = 12;
    public static final int TYPE_CUSTOM_AD = 13;
    public static final int TYPE_STARTAPP_AD = 14;
    public static final int TYPE_ADMOB = 15;
   // public static final int TYPE_REWARD = 16;

    private List<MainModel> mPayModels;

    private MainAdapterCallback callback;

    private PlanViewHolder planViewHolder;
  //  public RewardViewHolder rewardViewHolder;

    public static class MainModel {

        public CustomAd customAd;
        public boolean isPlan = false;
       // public boolean isReward = false;
        public boolean adMob = false;

      //  public NativeAdDetails startappAd;
//        public com.appodeal.ads.NativeAd appodealAd;

        public static MainModel createPlan() {
            MainModel mainModel = new MainModel();
            mainModel.customAd = null;
            mainModel.isPlan = true;

            return mainModel;
        }

        public static MainModel createCustomAd(CustomAd customAd) {
            MainModel mainModel = new MainModel();
            mainModel.customAd = customAd;
            mainModel.isPlan = false;

            return mainModel;
        }

//        public static MainModel createStartapp(NativeAdDetails startappAd) {
//            MainModel mainModel = new MainModel();
//            mainModel.startappAd = startappAd;
//            mainModel.isPlan = false;
//
//            return mainModel;
//        }

        public static MainModel createAdMobNative() {
            MainModel mainModel = new MainModel();
            mainModel.adMob = true;
            mainModel.isPlan = false;

            return mainModel;
        }

//        public static MainModel createRewardAd() {
//            MainModel mainModel = new MainModel();
//            mainModel.isReward = true;
//
//            return mainModel;
//        }

//        public static MainModel createAppodeal(com.appodeal.ads.NativeAd appodealAd) {
//            MainModel mainModel = new MainModel();
//            mainModel.appodealAd = appodealAd;
//            mainModel.isPlan = false;
//
//            return mainModel;
//        }
    }

    public interface MainAdapterCallback {
        void onCustomAdClicked(String pack);
      //  void onRewardAdClicked();
        void tvUpgradeCLicked();
    }

    public MainAdapter(MainAdapterCallback callback) {
        this.callback = callback;
    }

    public void setItems(List<MainModel> mPayModels) {
        this.mPayModels = mPayModels;
        notifyDataSetChanged();
    }

    public void addItem(MainModel mainModel){
        mPayModels.add(2, mainModel);
        notifyItemInserted(2);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_CUSTOM_AD:
                return new CustomAdViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_item, parent, false));
//            case TYPE_STARTAPP_AD:
//                return new StartAppAdViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_item, parent, false));
            case TYPE_ADMOB:
                return new AdMobViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.admob_item, parent, false));
//            case TYPE_REWARD:
//                rewardViewHolder = new RewardViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.reward_item, parent, false));
//                return rewardViewHolder;
            case TYPE_PLAN:
                planViewHolder = new PlanViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.plan_item, parent, false));
                return planViewHolder;
        }

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case TYPE_CUSTOM_AD:
                ((CustomAdViewHolder) holder).bind(mPayModels.get(position).customAd);
                break;

            case TYPE_PLAN:
                ((PlanViewHolder) holder).bind();
                break;

            case TYPE_ADMOB:
                ((AdMobViewHolder) holder).bind();
                break;

//            case TYPE_REWARD:
//                ((RewardViewHolder) holder).bind();
//                break;

//            case TYPE_STARTAPP_AD:
//                ((StartAppAdViewHolder) holder).bind(mPayModels.get(position).startappAd);
//                break;
        }
    }

//    public void onRestartRewardTimer() {
//        rewardViewHolder.initTimer();
//    }

    public void updatePlan() {
        if (planViewHolder != null) {
            planViewHolder.bind();
        }
    }

    public class PlanViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvUpgrade)
        protected TextView tvUpgrade;

        @BindView(R.id.tvCurPlanValue)
        protected TextView tvCurPlanValue;

        @BindView(R.id.tvEarnRateValue)
        protected TextView tvEarnRateValue;

        public PlanViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind() {
            int plan = App.getCurrentUser().plan;

            switch (plan) {
                case User.PLAN_DEF:
                    tvCurPlanValue.setText(R.string.plan_basic);
                    tvEarnRateValue.setText(R.string.plan_basic_val);
                    break;
                case User.PLAN_PREMIUM_DEMO:
                    tvCurPlanValue.setText(R.string.plan_premium_demo);
                    tvEarnRateValue.setText(R.string.plan_premium_val);
                    break;
                case User.PLAN_PREMIUM:
                    tvCurPlanValue.setText(R.string.plan_premium);
                    tvEarnRateValue.setText(R.string.plan_premium_val);
                    break;
                case User.PLAN_PLATINUM:
                    tvCurPlanValue.setText(R.string.plan_platinum);
                    tvEarnRateValue.setText(R.string.plan_platinum_val);
                    break;
            }
        }

        @OnClick(R.id.tvUpgrade)
        public void tvUpgradeCLicked() {
            callback.tvUpgradeCLicked();
        }
    }

    public class CustomAdViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        protected TextView title;

        @BindView(R.id.tvDescr)
        protected TextView tvDescr;

        @BindView(R.id.ivIcon)
        protected ImageView ivIcon;

        @BindView(R.id.tvSponsored)
        protected TextView sponsorred;

        @BindView(R.id.btn_install)
        protected Button installButton;

        @BindView(R.id.flContainerInteraction)
        protected FrameLayout flContainerInteraction;

        @BindView(R.id.parent)
        protected ViewGroup parent;

        @BindView(R.id.rr1)
        protected FrameLayout rr1;

        public CustomAdViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final CustomAd offer) {
            title.setText(offer.getTitle());
            tvDescr.setText(offer.getDescr());
            monk.helper.Utility.addIconAd(rr1);
            Picasso.with(ivIcon.getContext()).cancelRequest(ivIcon);
            Picasso.with(ivIcon.getContext())
                    .load(AwsApp.getLibConfigs().getDomain() + offer.getIcon())
                    .into(ivIcon);

            installButton.setText(R.string.install);
            installButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callback.onCustomAdClicked(offer.getPackageName());
                }
            });
        }
    }

//    public class RewardViewHolder extends RecyclerView.ViewHolder {
//
//        @BindView(R.id.flRewardAdLayout)
//        protected FrameLayout flRewardAdLayout;
//
//        @BindView(R.id.tvTimer)
//        protected TextView tvTimer;
//
//        @BindView(R.id.ivPlay)
//        protected ImageView ivPlay;
//
//        private Timer timer;
//
//        public RewardViewHolder(View itemView) {
//            super(itemView);
//            ButterKnife.bind(this, itemView);
//        }
//
//        public void bind() {
//          //  AdmobRewardHelper.preload(itemView.getContext());
//            tvTimer.setText(itemView.getResources().getString(R.string.watch_wideo, Double.toString(Constants.VIDEO_REWARD)));
//
//
//            ivPlay.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    callback.onRewardAdClicked();
//                }
//            });
//            initTimer();
//        }
//
//        public void initTimer() {
//            clearTimer();
//            timer = new Timer();
//            timer.schedule(new TimerTick(), 0,1000);
//        }
//
//        public void clearTimer() {
//            if (timer != null) {
//                timer.cancel();
//                timer.purge();
//            }
//        }

//        private class TimerTick extends TimerTask {
//
//            @Override
//            public void run() {
//                Activity activity = (Activity)itemView.getContext();
//                activity.runOnUiThread((new Runnable() {
//
//                    @Override
//                    public void run() {
//                        int cooldownReward = Constants.TIME_AD_REWARDS_SEC;
//                        long leftTime = App.getCurrentUser().lastWatchingAdd - miner.bitcoin.helper.Utility.getTime() + cooldownReward;
//                        long lastWatchingAdd = App.getCurrentUser().lastWatchingAdd;
//                        String watchVideoText = itemView.getResources().getString(R.string.watch_wideo, Double.toString(Constants.VIDEO_REWARD));
//                        String watchVideoLeftText = itemView.getResources().getString(R.string.watch_wideo_left, TimeConverter.convert((leftTime/1000)));
//                        if (leftTime > 0 ) {
//                            tvTimer.setText(watchVideoText + "\n" + watchVideoLeftText);
//                            ivPlay.setAlpha(0.2f);
//                            ivPlay.setClickable(false);
//                            ivPlay.setEnabled(false);
//                        }
//                        else {
//                            tvTimer.setText(watchVideoText);
//                            timer.cancel();
//                            timer.purge();
//                            ivPlay.setAlpha(1.0f);
//                            ivPlay.setClickable(true);
//                            ivPlay.setEnabled(true);
//                        }
//                    }
//                }));
//            }
//        }
//
//    }



//    public class StartAppAdViewHolder extends RecyclerView.ViewHolder {
//
//        @BindView(R.id.tvTitle)
//        protected TextView title;
//
//        @BindView(R.id.tvDescr)
//        protected TextView tvDescr;
//
//        @BindView(R.id.ivIcon)
//        protected ImageView ivIcon;
//
//        @BindView(R.id.tvSponsored)
//        protected TextView sponsorred;
//
//        @BindView(R.id.btn_install)
//        protected Button installButton;
//
//        @BindView(R.id.flContainerInteraction)
//        protected FrameLayout flContainerInteraction;
//
//        @BindView(R.id.parent)
//        protected ViewGroup parent;
//
//        public StartAppAdViewHolder(View itemView) {
//            super(itemView);
//            ButterKnife.bind(this, itemView);
//        }
//
//        public void bind(final NativeAdDetails nativeAd) {
//            title.setText(nativeAd.getTitle());
//            tvDescr.setText(nativeAd.getDescription());
//
//            Picasso.with(ivIcon.getContext()).cancelRequest(ivIcon);
//            Picasso.with(ivIcon.getContext())
//                    .load(nativeAd.getSecondaryImageUrl())
//                    .into(ivIcon);
//
//            installButton.setText(R.string.install);
//            installButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    nativeAd.sendClick(itemView.getContext());
//                }
//            });
//        }
//    }

    public class AdMobViewHolder extends RecyclerView.ViewHolder {

//        @BindView(R.id.parent)
//        protected ViewGroup parent;

        @BindView(R.id.flNativeAdLayout)
        protected FrameLayout flNativeAdLayout;

      // protected NativeWidgetHelper banner;

        public AdMobViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
           // banner = NativeWidgetHelper.create(flNativeAdLayout, NativeWidgetHelper.Ads.ADMOB);

        }

        public void bind() {

        }
    }


    @Override
    public int getItemViewType(int position) {
        if (mPayModels.get(position).isPlan) {
            return TYPE_PLAN;
        }

//        if (mPayModels.get(position).startappAd != null){
//            return TYPE_STARTAPP_AD;
//        }

        if (mPayModels.get(position).adMob){
            return TYPE_ADMOB;
        }
//
//        if (mPayModels.get(position).isReward){
//            return TYPE_REWARD;
//        }

        return TYPE_CUSTOM_AD;
    }

    @Override
    public int getItemCount() {
        return mPayModels == null ? 0 : mPayModels.size();
    }

}




