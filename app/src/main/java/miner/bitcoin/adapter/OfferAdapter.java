package miner.bitcoin.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import miner.bitcoin.R;
import monk.AwsApp;
import monk.model.Offer;
import monk.model.modelhelper.OffersHelper;

public class OfferAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TIME_OFFER_SHOWN_TIME = 5;
    public static final int OFFER = 12;

    private List<Offer> mPayModels;
    private boolean networkAvailiable;

    private PayAdapterCallback callback;

    public interface PayAdapterCallback {
        void onOfferClicked(Offer offer);
    }

    public OfferAdapter(PayAdapterCallback callback) {
        this.callback = callback;
        mPayModels = loadOffers();
    }

    private List<Offer> loadOffers() {
        List<Offer> offers = AwsApp.getOffers();

        if (offers != null) {
            Collections.shuffle(offers);
        }

        List<Offer> myOffers = new ArrayList<>();

        int counter = 0;

        if (offers != null && offers.size() > 0) {
            for (Offer offer : offers) {
                if (offer.getShownCount() < TIME_OFFER_SHOWN_TIME && !offer.getIsClicked()) {

                    if (counter >= 4) {
                        break;
                    }

                    OffersHelper.notifyShown(offer);
                    myOffers.add(offer);
                    ++counter;
                }
            }
        }

        return myOffers;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case OFFER:
                return new OfferViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.offer_item, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        int type = getItemViewType(position);
        switch (type) {
            case OFFER:
                ((OfferViewHolder) holder).bind(mPayModels.get(position));
                break;
        }
    }

    public class OfferViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvTitle)
        protected TextView title;

        @BindView(R.id.tvDescr)
        protected TextView tvDescr;

        @BindView(R.id.ivIcon)
        protected ImageView ivIcon;

        @BindView(R.id.tvSponsored)
        protected TextView sponsorred;

        @BindView(R.id.btn_install)
        protected Button installButton;

        @BindView(R.id.flContainerInteraction)
        protected FrameLayout flContainerInteraction;

        @BindView(R.id.parent)
        protected ViewGroup parent;

        public OfferViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bind(final Offer offer) {
            sponsorred.setVisibility(View.GONE);
            title.setText(offer.getTitle());
            tvDescr.setText(offer.getDescr());

            Picasso.with(ivIcon.getContext()).cancelRequest(ivIcon);
            Picasso.with(ivIcon.getContext())
                    .load(AwsApp.getLibConfigs().getDomain() + offer.getIcon())
                    .into(ivIcon);

            installButton.setText(offer.getBtnText());
            installButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mPayModels.remove(offer);
                    notifyItemRemoved(getAdapterPosition());

                    callback.onOfferClicked(offer);
                }
            });

            setEnabled(networkAvailiable);
        }

        public void setEnabled(boolean enabled) {
            if (enabled) {
                installButton.setEnabled(true);
                installButton.setSelected(false);
                installButton.setClickable(true);
            } else {
                installButton.setEnabled(false);
                installButton.setSelected(true);
                installButton.setClickable(false);
            }
        }
    }


    @Override
    public int getItemViewType(int position) {
        return OFFER;
    }

    public void networkAvailable(boolean availiable) {
        networkAvailiable = availiable;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mPayModels == null ? 0 : mPayModels.size();
    }

}



