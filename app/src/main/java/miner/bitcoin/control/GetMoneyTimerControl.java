package miner.bitcoin.control;

import android.view.View;
import android.widget.TextView;

public class GetMoneyTimerControl {

    private TextView tvCounter;
    private View viewCounter;
    private View btnGetBtc;

    private GetMoneyCallback timerCallback;

    private PureTimerControl pureTimerControl;

    public interface GetMoneyCallback {
        long getTimePassed();

        void saveAdTime();
    }

    public GetMoneyTimerControl(TextView tvCounter, View viewCounter, View viewCover, final GetMoneyCallback timerCallback) {
        this.tvCounter = tvCounter;
        this.btnGetBtc = viewCover;
        this.viewCounter = viewCounter;
        this.timerCallback = timerCallback;

        init();
    }

    private void init(){
        pureTimerControl = new PureTimerControl(tvCounter, new PureTimerControl.TimerCallback() {
            @Override
            public long getTimePassed() {
                return timerCallback.getTimePassed();
            }

            @Override
            public void onShowCoverView() {
                btnGetBtc.setVisibility(View.VISIBLE);
                tvCounter.setVisibility(View.GONE);
                viewCounter.setVisibility(View.GONE);
            }

            @Override
            public void onShowTimerView() {
                btnGetBtc.setVisibility(View.GONE);
                tvCounter.setVisibility(View.VISIBLE);
                viewCounter.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFinished() {}
        });
    }

    public void onGetRewardClicked() {
        timerCallback.saveAdTime();

        start();
    }

    public void setTimer(long time){
        pureTimerControl.setTimer(time);
    }

    public long getTimer() {
        return pureTimerControl.getTimer();
    }

    public void start() {
        pureTimerControl.start();
    }

    public void stop() {
        pureTimerControl.stop();
    }
}
