package miner.bitcoin.control;

import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import code.junker.JunkProvider;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.helper.Constants;
import monk.helper.Utility;

public class InAppNotificationControl {

    @BindView(R.id.llNotif)
    protected LinearLayout llNotif;

    @BindView(R.id.tvReward)
    protected TextView tvReward;

    private Random random = new Random();
    private static DecimalFormat df2 = new DecimalFormat("#.#####");

    public InAppNotificationControl(LinearLayout parent){
        ButterKnife.bind(this, parent);
    }

    public void show(){
        if (!Utility.hasInternet(App.getContext())){
            return;
        }
        JunkProvider.f();

        llNotif.setVisibility(View.VISIBLE);
        String userId = "0" + (10000 + random.nextInt(10000));
        float payout = (float)Constants.minPayoutCrypt + (random.nextInt(2))/10;

        tvReward.setText(tvReward.getContext().getString(R.string.riched_payout, userId, Double.toString(payout)) + " " + Constants.CRYPT_CODE);
        App.getUIHandler().postDelayed(new Runnable() {
            @Override
            public void run() {
                llNotif.setVisibility(View.GONE);
            }
        }, 4500);
    }

    @OnClick(R.id.llNotif)
    public void onllNotifClick(){
        llNotif.setVisibility(View.GONE);
    }

}
