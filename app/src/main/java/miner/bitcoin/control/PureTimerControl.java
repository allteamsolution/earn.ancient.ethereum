package miner.bitcoin.control;

import android.os.CountDownTimer;
import android.util.Log;
import android.widget.TextView;

public class PureTimerControl {
    private final static long DEF_TIMER = 15 * 1000;

    private TextView tvCounter;

    private long timer = DEF_TIMER;

    private CountDownTimer countDownTimer;

    private TimerCallback timerCallback;

    public interface TimerCallback{
        long getTimePassed();

        void onShowCoverView();

        void onShowTimerView();

        void onFinished();
    }

    public PureTimerControl(TextView tvCounter, TimerCallback timerCallback) {
        this.tvCounter = tvCounter;
        this.timerCallback = timerCallback;
    }

    public void setTimer(long time){
        timer = time;
    }

    public void start() {
        Log.d("Timer", "" + timerCallback.getTimePassed());

        if (timerCallback.getTimePassed() < 0) {
            timerCallback.onShowCoverView();
        } else {
            timerCallback.onShowTimerView();

            long time = Math.min(timer, timerCallback.getTimePassed());
            Log.d("Timer ", "min second: " + timerCallback.getTimePassed());

            countDownTimer = new CountDownTimer(time, 1000) {

                public void onTick(long millisUntilFinished) {
                    Log.d("Timer", "timeleft " + millisUntilFinished);
                    int seconds = (int) (millisUntilFinished / 1000) % 60;
                    int minutes = (int) ((millisUntilFinished / (1000 * 60)) % 60);
//                    int hours = (int) ((millisUntilFinished / (1000 * 60 * 60)) % 24);

                    //format(hours) + " : " +
                    String time = format(minutes) + " : " + format(seconds);
                    tvCounter.setText(time);
                }

                public void onFinish() {
                    timerCallback.onShowCoverView();
                    timerCallback.onFinished();
                }
            };
            countDownTimer.start();
        }
    }

    public void stop() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    public long getTimer() {
        return timer;
    }

    private String format(int i) {
        if (i < 10) {
            return "0" + i;
        }

        return "" + i;
    }
}
