package miner.bitcoin.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import butterknife.ButterKnife;
import miner.bitcoin.R;

public class BoughtItemDialog extends BaseDialogHelper{

    public static void show(Context context){
        new BoughtItemDialog(context);
    }

    private BoughtItemDialog(Context context){
        super(context);
    }

    @Override
    protected View onCreateView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.bought_dlg, null);
        ButterKnife.bind(this, view);

        return view;
    }


}
