package miner.bitcoin.dialog;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.helper.Utility;

public class PrivacyDialog extends BaseDialogHelper {

    @BindView(R.id.cbPrivacy)
    protected CheckBox cbPrivacy;

    @BindView(R.id.btnAgree)
    protected Button btnAgree;

    private PrivacyDialogCallback callback;

    public static void show(Context context, PrivacyDialogCallback callback) {
        new PrivacyDialog(context,callback);
    }

    private PrivacyDialog(Context context, PrivacyDialogCallback callback) {
        super(context);
        this.callback = callback;
    }

    public interface PrivacyDialogCallback{
        void onOpenPrivacy(String url);
    }

    @Override
    protected View onCreateView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.privacy_dlg, null);
        ButterKnife.bind(this, view);

        cbPrivacy.setButtonDrawable(Utility.getCheckBoxDrawable());
        cbPrivacy.setChecked(true);
        initPrivacy();

        return view;
    }

    private void initPrivacy(){
        String str0 = App.getContext().getResources().getString(R.string.agree_privacy);
        String str1 = App.getContext().getResources().getString(R.string.agree_privacy_suff);

        ForegroundColorSpan spanWhite = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.primary_text));
        ForegroundColorSpan spanFab = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.fabcolor));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                callback.onOpenPrivacy(App.getContext().getResources().getString(R.string.privacy_url));
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        int size = str0.length();
        str0 += " " + str1;
        Spannable spannable = new SpannableString(str0);
        spannable.setSpan(spanWhite, 0, size, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannable.setSpan(clickableSpan, size, str0.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(spanFab, size, str0.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        cbPrivacy.setText(spannable);
        cbPrivacy.setMovementMethod(LinkMovementMethod.getInstance());
        cbPrivacy.setHighlightColor(Color.TRANSPARENT);
        cbPrivacy.setChecked(true);
    }

    @OnClick(R.id.btnAgree)
    public void btnAgreeClicked(){
        if (cbPrivacy.isChecked()) {
            App.getCurrentUser().privacyAgreed = true;
            App.setCurrentUser(App.getCurrentUser());

            dismiss();
        } else {
            dismiss();
        }
    }

    @OnCheckedChanged(R.id.cbPrivacy)
    public void cbPrivacyChechChange(){}

}

