package miner.bitcoin.dialog;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.R;

public class RateUsHelper extends BaseDialogHelper {

    private RateUsHelperCallback callback;

    public interface RateUsHelperCallback{
        void onGoneToGooglePlay();
    }

    public static void show(Context context, RateUsHelperCallback callback) {
        new RateUsHelper(context,callback);
    }

    private RateUsHelper(Context context, RateUsHelperCallback callback) {
        super(context);
        this.callback = callback;
    }

    public interface PrivacyDialogCallback{
        void onOpenPrivacy(String url);
    }

    @Override
    protected View onCreateView() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.rate_on_gp, null);
        ButterKnife.bind(this, view);

        return view;
    }

    @OnClick(R.id.btnAgree)
    public void onbtnAgreeClick(){
        callback.onGoneToGooglePlay();
        dismiss();
    }
}
