package miner.bitcoin.event;

public class MinedEvent {

    public float minedValue;

    public MinedEvent(float minedValue){
        this.minedValue = minedValue;
    }
}
