package miner.bitcoin.helper;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.IntRange;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.BounceInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;
import android.widget.TextView;

import static android.animation.ObjectAnimator.ofFloat;

public class AnimationFactory {
    public static final int PROGRESS_ACCURACY = 2;
    public static final long DEFAULT_DURATION = 400;
    public static final long SCALE_DURATION = 300;
    public static final long FILL_BAR_DURATION = 1200;
    public static final long COLOR_CHANGE_ANIMATION = 600;
    public static final long SLIDE_DURATION = 1500;
    public static final long FLIGHT_DURATION = 400;
    public static final long BOUNCE_DURATION = 15;
    public static final long ROTATE_DURATION = 2000;
    public static final long FADE_OUT_DURATION = 2000;

    private static ValueAnimator sCurrentStatusBarAnimator;

    public static Animator scaleView(final View v, float from, float to) {
        ObjectAnimator scaleStickerXAnim = ofFloat(v, View.SCALE_X, from, to);
        ObjectAnimator scaleStickerYAnim = ofFloat(v, View.SCALE_Y, from, to);
        AnimatorSet set = new AnimatorSet();
        set.setInterpolator(new BounceInterpolator());
        set.playTogether(scaleStickerXAnim, scaleStickerYAnim);
        set.setDuration(SCALE_DURATION);
        return set;
    }


    public static ValueAnimator fillProgress(final ProgressBar progressBar,
                                             final TextView textView,
                                             @IntRange(from = 0, to = 100) int start,
                                             @IntRange(from = 0, to = 100) int end) {
        progressBar.setMax(100 * PROGRESS_ACCURACY);
        end *= PROGRESS_ACCURACY;
        ValueAnimator animator = ObjectAnimator.ofInt(start, end)
                .setDuration(FILL_BAR_DURATION);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int progress = (int) animation.getAnimatedValue();
                progressBar.setProgress(progress);
                textView.setText(String.format("%1d%%", progress / PROGRESS_ACCURACY));
            }
        });
        return animator;
    }


    public static ValueAnimator changeBackgroundColor(final View view,
                                                      @ColorInt int startColor,
                                                      @ColorInt int endColor) {
        ValueAnimator animator = ValueAnimator.ofObject(new ArgbEvaluator(), startColor, endColor)
                .setDuration(COLOR_CHANGE_ANIMATION);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                view.setBackgroundColor((int) animator.getAnimatedValue());
            }
        });
        return animator;
    }

    public static ValueAnimator changeBackgroundColorRes(View view,
                                                         @ColorRes int startColor,
                                                         @ColorRes int endColor) {
        int colorFrom = ContextCompat.getColor(view.getContext(), startColor);
        int colorTo = ContextCompat.getColor(view.getContext(), endColor);
        return changeBackgroundColor(view, colorFrom, colorTo);
    }

    public static ValueAnimator changeTextColor(final TextView textView,
                                                @ColorInt int startColor,
                                                @ColorInt int endColor) {
        ValueAnimator animator = ValueAnimator.ofObject(new ArgbEvaluator(), startColor, endColor)
                .setDuration(COLOR_CHANGE_ANIMATION);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                textView.setTextColor((int) animator.getAnimatedValue());
            }
        });
        return animator;
    }

    public static ValueAnimator changeTextColorRes(final TextView textView,
                                                   @ColorRes int startColor,
                                                   @ColorRes int endColor) {
        int colorFrom = ContextCompat.getColor(textView.getContext(), startColor);
        int colorTo = ContextCompat.getColor(textView.getContext(), endColor);
        return changeTextColor(textView, colorFrom, colorTo);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static ValueAnimator changeStatusBarColor(final Activity activity,
                                                     @ColorInt int startColor,
                                                     @ColorInt int endColor) {
        if (sCurrentStatusBarAnimator != null &&
                (sCurrentStatusBarAnimator.isRunning() || sCurrentStatusBarAnimator.isStarted())) {
            sCurrentStatusBarAnimator.cancel();
            sCurrentStatusBarAnimator = null;
        }
        final ValueAnimator animator = ValueAnimator.ofObject(new ArgbEvaluator(), startColor, endColor)
                .setDuration(COLOR_CHANGE_ANIMATION);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        final Window window = activity.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                window.setStatusBarColor((int) animator.getAnimatedValue());
            }
        });
        animator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationStart(Animator animation) {
                sCurrentStatusBarAnimator = animator;
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                sCurrentStatusBarAnimator = null;
            }
        });
        return animator;
    }

    public static ValueAnimator changeStatusBarColorRes(final Activity activity,
                                                        @ColorRes int startColor,
                                                        @ColorRes int endColor) {
        int colorFrom = ContextCompat.getColor(activity, startColor);
        int colorTo = ContextCompat.getColor(activity, endColor);
        return changeStatusBarColor(activity, colorFrom, colorTo);
    }

    public static ObjectAnimator slideRight(View view, int px) {
        return ofFloat(view, View.TRANSLATION_X,
                view.getTranslationX(), view.getTranslationX() + px)
                .setDuration(SLIDE_DURATION);
    }

    public static ObjectAnimator slideLeft(View view, int px) {
        return ofFloat(view, View.TRANSLATION_X,
                view.getTranslationX(), view.getTranslationX() - px)
                .setDuration(SLIDE_DURATION);
    }

    public static ObjectAnimator slideUp(View view, int px) {
        return ofFloat(view, View.TRANSLATION_Y,
                view.getTranslationY(), view.getTranslationY() - px)
                .setDuration(SLIDE_DURATION);
    }

    public static ObjectAnimator slideDown(View view, int px) {
        return ofFloat(view, View.TRANSLATION_Y,
                view.getTranslationY(), view.getTranslationY() + px)
                .setDuration(SLIDE_DURATION);
    }

    public static ValueAnimator scaleWidth(final View view, float fromPx, float toPx) {
        ObjectAnimator scaleStickerXAnim = ofFloat(view, View.SCALE_X, fromPx, toPx);
        scaleStickerXAnim.setInterpolator(new AccelerateInterpolator());
        return scaleStickerXAnim;
    }

    public static ObjectAnimator slideUpDown(View view, int px) {
        ObjectAnimator animator = ofFloat(view, View.TRANSLATION_Y, view.getTranslationY() + px, view.getTranslationY() - px)
                .setDuration(SLIDE_DURATION / 2);
        animator.setInterpolator(new AccelerateDecelerateInterpolator());
        return animator;
    }

    public static ValueAnimator changeHeight(final View view, int fromPx, int toPx) {
        ValueAnimator animator = ValueAnimator.ofInt(fromPx, toPx)
                .setDuration(1000);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                view.getLayoutParams().height = (int) animation.getAnimatedValue();
                view.requestLayout();
            }
        });
        return animator;
    }

    public static Animator bounce(View view, float ratio) {
        int diff = (int) (view.getWidth() * ratio);
        ObjectAnimator rightSlideAnimator = ofFloat(view, View.TRANSLATION_X, 0, diff)
                .setDuration(BOUNCE_DURATION);
        ObjectAnimator leftSlideAnimator = ofFloat(view, View.TRANSLATION_X, diff, -diff)
                .setDuration(BOUNCE_DURATION * 2);
        ObjectAnimator centerSlideAnimator = ofFloat(view, View.TRANSLATION_X, -diff, 0)
                .setDuration(BOUNCE_DURATION);
        AnimatorSet set = new AnimatorSet();
        set.playSequentially(rightSlideAnimator, leftSlideAnimator, centerSlideAnimator);
        set.setInterpolator(new LinearInterpolator());
        return set;
    }

    public static ObjectAnimator rotate(View view, float from, float to) {
        ObjectAnimator animator = ofFloat(view, View.ROTATION, from, to)
                .setDuration(ROTATE_DURATION);
        LinearInterpolator interpolator = new LinearInterpolator();
        animator.setInterpolator(interpolator);
        return animator;
    }

    public static ObjectAnimator fadeOut(View view) {
        ObjectAnimator animator = ofFloat(view, View.ALPHA, 1, 0)
                .setDuration(FADE_OUT_DURATION);
        animator.setInterpolator(new Interpolator() {
            @Override
            public float getInterpolation(float input) {
                return input * input;
            }
        });
        return animator;
    }

}

