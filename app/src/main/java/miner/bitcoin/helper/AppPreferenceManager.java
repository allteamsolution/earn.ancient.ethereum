package miner.bitcoin.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import miner.bitcoin.App;
import miner.bitcoin.R;

public class AppPreferenceManager {

    private static AppPreferenceManager instance;

    private final SharedPreferences mPrefs;

    private AppPreferenceManager() {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(App.getContext());
    }

    public static synchronized AppPreferenceManager getInstance(){
        if (instance == null){
            instance = new AppPreferenceManager();
        }

        return instance;
    }

    public boolean getIsFirstLaunch(){
        return mPrefs.getBoolean(getContext().getString(R.string.is_first_launch), true);
    }

    public void setLastTextShown(Context context, int number) {
        PreferenceManager.getDefaultSharedPreferences(context)
                .edit()
                .putInt("text", number)
                .apply();
    }

    public int getLastTextShown(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt("text", 0);
    }

    public void setLastTimeNotification(long time) {
        PreferenceManager.getDefaultSharedPreferences(App.getContext())
                .edit()
                .putLong("notificationTime", time)
                .apply();
    }

    public long getLastTimeNotification() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext())
                .getLong("notificationTime", 0);
    }


    public void setLastRateWidgetShownTime(long time) {
        PreferenceManager.getDefaultSharedPreferences(App.getContext())
                .edit()
                .putLong("jknfjernfewfk", time)
                .apply();
    }

    public long getLastRateWidgetShownTime() {
        return PreferenceManager.getDefaultSharedPreferences(App.getContext())
                .getLong("jknfjernfewfk", 0);
    }

    public void setNotFirstLaunch(){
        putBoolean(getContext().getString(R.string.is_first_launch), false);
    }

    public boolean getIsTutorialEveryTime(){
        return mPrefs.getBoolean(getContext().getString(R.string.sp_tutorail), false);
    }

    public void setTutorialEveryTime(boolean b){
        putBoolean(getContext().getString(R.string.sp_tutorail), b);
    }


    public void setConsent(boolean consent){
        putBoolean("GDPR-Consent", consent);
    }

    public boolean getConsent(){
        return mPrefs.getBoolean("GDPR-Consent", true);
    }

    private Context getContext(){
        return App.getContext();
    }

    private void putBoolean(@NonNull String name, boolean value) {
        mPrefs.edit().putBoolean(name, value).apply();
    }

    private void putInt(@NonNull String name, int value) {
        mPrefs.edit().putInt(name, value).apply();
    }

    private void putLong(@NonNull String name, long value) {
        mPrefs.edit().putLong(name, value).apply();
    }

    private void putString(@NonNull String name, @Nullable String value) {
        mPrefs.edit().putString(name, value).apply();
    }
}
