package miner.bitcoin.helper;


import miner.bitcoin.App;

public class GDPRHelper {

    public static void sendUserConsent(boolean consent) {
        sendStartAppConsent(consent);
        sendApplovinConsent(consent);
    }

    private static void sendStartAppConsent(boolean consent) {
        //StartAppSDK.setUserConsent(App.getContext(), "EULA", System.currentTimeMillis(), consent);
    }

    private static void sendApplovinConsent(boolean consent){
        //AppLovinPrivacySettings.setHasUserConsent(consent, App.getContext());
    }
}
