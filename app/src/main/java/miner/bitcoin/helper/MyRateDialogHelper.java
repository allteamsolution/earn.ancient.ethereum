package miner.bitcoin.helper;

import android.app.Activity;

import miner.bitcoin.App;
import miner.bitcoin.R;
import rateusdialoghelper.RateDialogHelper;

public class MyRateDialogHelper {

    public static void show(Activity activity){
        int colorInactive = App.getContext().getResources().getColor(R.color.colorPrimaryDark);
        int colorActive = App.getContext().getResources().getColor(R.color.colorAccent);

        RateDialogHelper myRateDialogHelper = new RateDialogHelper.Builder()
                .setRatingColorActive(colorActive)
                .setRatingColorInactive(colorInactive)
                .setTitleAppNameColor(colorActive)
                .setCancelColor(colorInactive)
                .setAppName(App.getContext().getResources().getString(R.string.app_name))
                .setRateColor(colorActive)
                .setFeedbackEmail("youremail@gmail.com")
                .setDayAmount(2)
                .setSessionAmount(5)
                .build();

        myRateDialogHelper.showRateDialog(activity);
    }
}
