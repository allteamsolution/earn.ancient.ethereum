package miner.bitcoin.helper;

import java.util.Random;

public class RefCodeGenerator {

    public static final int REF_CODE_LEN = 6;
    public static final int USER_ID_LEN = 20;
    public static final int TXS_LEN = 25;


    public static String generate(){
        int length = REF_CODE_LEN;
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }
        return result.toString().toUpperCase();
    }

    public static String generateUserId(){
        int length = USER_ID_LEN;
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {

            if (length == 4 || length == 8 || length == 12 || length == 16){
                result.append('-');
                length--;
                continue;
            }
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }

        return result.toString().toUpperCase();
    }

    public static String generateTxs(){
        int length = TXS_LEN;
        final String characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
        StringBuilder result = new StringBuilder();
        while(length > 0) {
            Random rand = new Random();
            result.append(characters.charAt(rand.nextInt(characters.length())));
            length--;
        }

        return result.toString().toUpperCase();
    }




}
