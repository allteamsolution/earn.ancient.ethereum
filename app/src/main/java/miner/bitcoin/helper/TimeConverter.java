package miner.bitcoin.helper;

import miner.bitcoin.App;
import miner.bitcoin.R;

public class TimeConverter {
    public static String convert(long seconds) {
        String result = "";
        if (seconds < 60) {
            result = seconds + " " +App.getContext().getResources().getString(R.string.sec);
        } else if (seconds > 60 && seconds < 3600) {
            result = Math.floor(seconds / 60) + " " +App.getContext().getResources().getString(R.string.sec);
        }

        return result;
    }
}
