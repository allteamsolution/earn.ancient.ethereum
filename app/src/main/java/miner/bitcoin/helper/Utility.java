package miner.bitcoin.helper;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.format.DateFormat;
import android.widget.ImageView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import miner.bitcoin.App;
import miner.bitcoin.R;

import static android.graphics.Paint.ANTI_ALIAS_FLAG;

public class Utility {

    public static final String INTENT_TYPE_TEXT_PLAIN = "text/plain";

    static public void setDrawableColor(Drawable drawable, int color) {
        drawable.mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    static public void setDrawableColor(ImageView iv, int color) {
        iv.getDrawable().mutate().setColorFilter(color, PorterDuff.Mode.SRC_IN);
    }

    public static Drawable getCheckBoxDrawable(){
        return MagicDrawable.createCheckBox(App.getContext().getResources().getDrawable(R.drawable.baseline_check_box_outline_blank_black_24),
                App.getContext().getResources().getColor(R.color.black),
                App.getContext().getResources().getDrawable(R.drawable.baseline_check_box_black_24),
                App.getContext().getResources().getColor(R.color.fabcolor));
    }

    public static boolean hasInternet(Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (activeNetwork == null) return false;
        return activeNetwork.isConnectedOrConnecting();
    }

    public static void share(Context context, boolean withRef){
        Intent intent = new Intent(Intent.ACTION_SEND);

        String text = "";
        String text0 = context.getResources().getString(R.string.share_text, context.getPackageName());
        String text1 = context.getResources().getString(R.string.share_text_middle, App.getCurrentUser().refCode);
        String text2 = context.getResources().getString(R.string.share_text_end);

        if (withRef) {
            text = text0 + text1 + text2;
        } else {
            text = text0 + text2;
        }
        intent.setType(INTENT_TYPE_TEXT_PLAIN);
        intent.putExtra(Intent.EXTRA_TEXT, text);

        String shareTitle = App.getContext().getResources().getString(R.string.share_chooser_title);
        if (withRef){
            shareTitle = App.getContext().getResources().getString(R.string.share_chooser_title_with_ref);
        }
        Intent chooser = Intent.createChooser(intent, shareTitle);

        if (intent.resolveActivity(App.getContext().getPackageManager()) != null) {
            context.startActivity(chooser);
        }
    }

    public static String formatBalance(float balance){
        String formattedString = String.format(Constants.FORMAT_BALANCE, balance);
        //formattedString = formattedString.substring(0, formattedString.length()-2) + "." + formattedString.substring(formattedString.length()-2, formattedString.length());
        return formattedString;
    }

    public static long getTime(){
        return System.currentTimeMillis();
    }

    public static String getDate(long time) {
        Calendar cal = Calendar.getInstance(Locale.ENGLISH);
        cal.setTimeInMillis(time);
        String date = DateFormat.format("dd-MM-yy", cal).toString();
        return date;
    }
}
