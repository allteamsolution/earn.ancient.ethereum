package miner.bitcoin.model;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import miner.bitcoin.App;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.RefCodeGenerator;

public class User {

    private static final int TIME_PREMIUM = 10 * 60 * 60 * 1000;

    public final static int PLAN_DEF = 1;
    public final static int PLAN_PREMIUM = 2;
    public final static int PLAN_PREMIUM_DEMO = 4;
    public final static int PLAN_PLATINUM = 3;

    final public static String BALANCE = "BALANCE";
    final public static String BALANCE_UNREAL = "BALANCE_UNREAL";
    final public static String BALANCE_UNREALTIME = "BALANCE_UNREALTIME";

    final public static String PLAN = "PLAN";
    final public static String TIME_LAST_RESTART = "TIME_LAST_RESTART";
    final public static String UI_REFCODE_APPLIED = "UI_REFCODE_APPLIED";
    final public static String MY_REFERRER = "MY_REFERRER";
    final public static String UI_REF_CODE = "refCode";
    final public static String UI_VALUE_WALLET = "wallet";
    final public static String UI_USER_ID = "UI_USER_ID";
    final public static String PRIVACY_AGREE = "PRIVACY_AGREE";
    final public static String TIME_DEMO_PREM = "TIME_DEMO_PREM";
    final public static String TIME_WATCH_REWARD = "TIME_WATCH_REWARD";
    final public static String IS_DEMO_PREM = "IS_DEMO_PREM";
    final public static String      TOKEN = "TOKEN";
    final public static String      SIGN_IN = "SIGN_IN";

    public float balance;

    public long timeLastRestart;

    public int plan;

    public String refCode;

    public String myReferrer;

    public boolean refCodeApplied;

    public boolean privacyAgreed;

    public String walletAddress;

    public String userId;

    public boolean isDemoPremiumUsed;
    public long timeDemoPremiumStart;
    public long lastWatchingAdd;

    public float balanceUserTryToWithdraw;
    public long balanceUserTryToWithdrawTime;
    public boolean signIn;
    public String token;


    public void increaseBalance(double reward) {
        this.balance += reward;
    }

    public float getPlanReward() {
        switch (plan) {
            case PLAN_DEF:
                return Constants.DEF_PLAN_REW;
            case PLAN_PREMIUM:
                return Constants.PREMIUM_PLAN_REW;
            case PLAN_PREMIUM_DEMO:
                if (isDemoPremiumActive()) {
                    return Constants.PREMIUM_PLAN_REW;
                } else {
                    plan = User.PLAN_DEF;
                    save();
                    return Constants.DEF_PLAN_REW;
                }
            case PLAN_PLATINUM:
                return Constants.PLATINUM_PLAN_REW;
        }

        return 0f;
    }

    public boolean isDemoPremiumActive(){
        if (timeDemoPremiumStart + TIME_PREMIUM > System.currentTimeMillis()){
            return true;
        }

        return false;
    }

    public void save() {
        final SharedPreferences.Editor storage = PreferenceManager.getDefaultSharedPreferences(App.getContext()).edit();
        storage.putFloat(BALANCE, balance);
        storage.putFloat(BALANCE_UNREAL, balanceUserTryToWithdraw);
        storage.putLong(BALANCE_UNREALTIME, balanceUserTryToWithdrawTime);
        storage.putString(UI_USER_ID, userId);

        storage.putInt(PLAN, plan);
        storage.putLong(TIME_LAST_RESTART, timeLastRestart);
        storage.putLong(TIME_DEMO_PREM, timeDemoPremiumStart);
        storage.putLong(TIME_WATCH_REWARD, lastWatchingAdd);

        storage.putString(UI_REF_CODE, refCode);
        storage.putString(MY_REFERRER, myReferrer);
        storage.putBoolean(UI_REFCODE_APPLIED, refCodeApplied);
        storage.putBoolean(IS_DEMO_PREM, isDemoPremiumUsed);

        storage.putBoolean(PRIVACY_AGREE, privacyAgreed);
        storage.putString(UI_VALUE_WALLET, walletAddress);

        storage.putString(TOKEN, token);
        storage.putBoolean(SIGN_IN, signIn);

        storage.apply();
    }

    public void load() {
        final SharedPreferences storage = PreferenceManager.getDefaultSharedPreferences(App.getContext());
        balance = storage.getFloat(BALANCE, 0L);
        balanceUserTryToWithdraw = storage.getFloat(BALANCE_UNREAL, 0L);
        balanceUserTryToWithdrawTime = storage.getLong(BALANCE_UNREALTIME, 0L);
        plan = storage.getInt(PLAN, PLAN_DEF);
        timeLastRestart = storage.getLong(TIME_LAST_RESTART, 0L);
        timeDemoPremiumStart = storage.getLong(TIME_DEMO_PREM, 0L);
        lastWatchingAdd = storage.getLong(TIME_WATCH_REWARD, 0L);

        refCode = storage.getString(UI_REF_CODE, RefCodeGenerator.generate());
        myReferrer = storage.getString(MY_REFERRER, "");
        refCodeApplied = storage.getBoolean(UI_REFCODE_APPLIED, false);
        privacyAgreed = storage.getBoolean(PRIVACY_AGREE, false);
        isDemoPremiumUsed =  storage.getBoolean(IS_DEMO_PREM, false);

        walletAddress = storage.getString(UI_VALUE_WALLET, "");
        userId = storage.getString(UI_USER_ID, RefCodeGenerator.generateUserId());

        signIn = storage.getBoolean(SIGN_IN, false);
        token = storage.getString(TOKEN, "");

    }
}
