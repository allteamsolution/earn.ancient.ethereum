package miner.bitcoin.provider;

import android.support.annotation.StringRes;

import java.util.ArrayList;
import java.util.List;

import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.helper.Constants;

public class FaqProvider {

    public static class FaqModel {
        public String question;
        public String answer;
    }

    public static List<FaqModel> provide() {
        List<FaqModel> list = new ArrayList<>();

        list.add(create(R.string.faq_q1, App.getContext().getResources().getString(R.string.faq_a1, Constants.CRYPT)));
        list.add(create(R.string.faq_q2, App.getContext().getResources().getString(R.string.faq_a2, ("" + Constants.minPayoutCrypt + " " + Constants.CRYPT_CODE))));
        list.add(create(R.string.faq_q3, App.getContext().getResources().getString(R.string.faq_a3, Constants.CRYPT)));
        list.add(create(R.string.faq_q4, R.string.faq_a4));
        list.add(create(R.string.faq_q5, R.string.faq_a5));
        list.add(create(R.string.faq_q6, R.string.faq_a6));
        list.add(create(R.string.faq_q7, R.string.faq_a7));

        List<FaqModel> moreList = Constants.getExtraFaqs();
        if (moreList != null){
            list.addAll(moreList);
        }

        return list;
    }

    public static FaqModel create(@StringRes int title, @StringRes int descr){
        FaqModel item = new FaqModel();
        item.question = App.getContext().getResources().getString(title);
        item.answer = App.getContext().getResources().getString(descr);

        return item;
    }

    public static FaqModel create(@StringRes int title, String descr){
        FaqModel item = new FaqModel();
        item.question = App.getContext().getResources().getString(title);
        item.answer = descr;

        return item;
    }
}
