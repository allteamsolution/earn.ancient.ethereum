package miner.bitcoin.rest;

import miner.bitcoin.rest.body.PullBalanceBody;
import miner.bitcoin.rest.body.PushBalanceBody;
import miner.bitcoin.rest.body.PushExactBalanceBody;
import miner.bitcoin.rest.response.BalanceResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface RestService {

    @POST("/api/crane/GetBalance")
    public Call<BalanceResponse> getBalance(@Body PullBalanceBody body);

    @POST("/api/crane/SetBalance")
    public Call<Void> setBalance(@Body PushBalanceBody body);

    @POST("/api/crane/SetExactBalance")
    public Call<Void> setExactBalance(@Body PushExactBalanceBody body);
}
