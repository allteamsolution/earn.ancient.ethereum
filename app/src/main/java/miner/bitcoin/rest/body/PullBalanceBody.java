package miner.bitcoin.rest.body;

import com.google.gson.annotations.SerializedName;

public class PullBalanceBody {

    @SerializedName("app")
    public String app;

    @SerializedName("token")
    public String token;

    public PullBalanceBody(String app, String token) {
        this.app = app;
        this.token = token;
    }
}
