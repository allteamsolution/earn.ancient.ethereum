package miner.bitcoin.rest.body;

import com.google.gson.annotations.SerializedName;

public class PushExactBalanceBody {
    @SerializedName("app")
    public String app;

    @SerializedName("token")
    public String token;

    @SerializedName("money")
    public double money;

    @SerializedName("spins")
    public int spins;

    public PushExactBalanceBody(String app, String token, double money, int spins) {
        this.app = app;
        this.token = token;
        this.money = money;
        this.spins = spins;
    }
}
