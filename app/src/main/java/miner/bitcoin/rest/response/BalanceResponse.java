package miner.bitcoin.rest.response;

import com.google.gson.annotations.SerializedName;

public class BalanceResponse {

    @SerializedName("success")
    public boolean success;

    @SerializedName("result")
    public Result result;

    public class Result{
        @SerializedName("money")
        public double money;

        @SerializedName("spins")
        public int spins;

        @SerializedName("date_registered")
        public int date_registered;
    }

}
