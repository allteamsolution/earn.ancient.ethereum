package miner.bitcoin.service;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Binder;
import android.os.Build;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

import miner.bitcoin.App;
import miner.bitcoin.activity.MainActivity;
import miner.bitcoin.R;
import miner.bitcoin.event.MineStartedEvent;
import miner.bitcoin.event.MineStoppedEvent;
import miner.bitcoin.event.MinedEvent;
import miner.bitcoin.helper.Constants;
import miner.bitcoin.helper.Utility;
import miner.bitcoin.receiver.NetworkStateReceiver;

public class ForegroundService extends Service implements NetworkStateReceiver.NetworkStateReceiverListener {

    private static final String channelId = App.getContext().getPackageName()+".channel";

    public static volatile boolean isRunning = false;
    public static final int TIME_RESTART_NEEDED = 1000 * 60 * 60 * 3;

    public static final String ACTION_START_MINING = "lsd_start_mining";
    public static final String ACTION_STOP_MINING = "lsd_stop_mining";

    private final IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public ForegroundService getService() {
            return ForegroundService.this;
        }
    }

    private static Intent createIntent() {
        return new Intent(App.getContext(), ForegroundService.class);
    }

    public static Intent createIntent(String action) {
        return createIntent().setAction(action);
    }

    public Runnable runnable = new Runnable() {
        @Override
        public void run() {

            Log.d("JJJJJJJJ", "System.currentTimeMillis() : " + Utility.getTime());
            Log.d("JJJJJJJJ", "App.getCurrentUser().tim() : " + App.getCurrentUser().timeLastRestart);

            if (Utility.getTime() - App.getCurrentUser().timeLastRestart > TIME_RESTART_NEEDED) {
                stopMiner();

                sendNotification(App.getStringById(R.string.miner_need_restart));
                return;
            }

            if (Utility.hasInternet(ForegroundService.this)) {
                float reward = App.getCurrentUser().getPlanReward();
                App.getCurrentUser().balance += reward;
                App.setCurrentUser(App.getCurrentUser());

                float newSatoshi = (float) (Constants.btcToSatoshi * reward);

                sendNotification(App.getStringById(R.string.mined_balance) + " " + Utility.formatBalance(App.getCurrentUser().balance) + " " + Constants.CRYPT_CODE);

                EventBus.getDefault().post(new MinedEvent(newSatoshi));
            } else {
                sendNotification(App.getStringById(R.string.no_inet_service));
            }

            App.getUIHandler().postDelayed(runnable, 1000);
        }
    };

    private void sendNotification(String text) {
        Notification notify = createNotification(text, !isRunning);
        NotificationManager nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        nm.notify(Constants.NOTIFICATION_CODE, notify);
    }

    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (intent != null && intent.getAction() != null && intent.getAction().equals(ACTION_START_MINING)) {
            startMiner();
        } else {
            stopMiner();
        }

        return START_STICKY;
    }

    private void startMiner() {
        isRunning = true;
        MiningEngine.startMining();

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O)
            startMyOwnForeground(createNotification(App.getStringById(R.string.miner_is_starting), !isRunning));
        else {
            startForeground(Constants.NOTIFICATION_CODE, createNotification(App.getStringById(R.string.miner_is_starting), !isRunning));
        }

        App.getUIHandler().postDelayed(runnable, 1000);

        EventBus.getDefault().post(new MineStartedEvent());
    }

    @TargetApi(26)
    private void startMyOwnForeground(Notification notification){
        String channelName = "yohoo service";
        NotificationChannel chan = new NotificationChannel(channelId, channelName, NotificationManager.IMPORTANCE_NONE);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        startForeground(Constants.NOTIFICATION_CODE, notification);
    }

    private void stopMiner() {
        isRunning = false;
        MiningEngine.stopMining();

        App.getUIHandler().removeCallbacks(runnable);
        stopForeground(true);
        stopSelf();

        EventBus.getDefault().post(new MineStoppedEvent());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isRunning = false;
    }

    private Notification createNotification(String contentText, boolean isStopped) {

        Intent activityIntent = new Intent(this, MainActivity.class);
        PendingIntent piShowActivity = PendingIntent.getActivity(this, 0, activityIntent, 0);
        final Bitmap icon = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
//        final Bitmap largeIcon = Bitmap.createScaledBitmap(icon, 128, 128, false);

        final Notification notification;
        if (isStopped) {
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle(getResources().getString(R.string.notification_title))
                    .setTicker("This is a ticker")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(contentText))
                    .setContentText(contentText)
                    .setLargeIcon(icon)
                    .setContentIntent(piShowActivity) // back to
                    .setOngoing(true)
                    .setChannelId(channelId)
                    .setAutoCancel(true) // just return to activity and dismiss
                    .build();
        } else {
            Intent serviceIntent = new Intent(this, ForegroundService.class);
            serviceIntent.setAction(ACTION_STOP_MINING);
            PendingIntent piStopMining = PendingIntent.getService(this, 0, serviceIntent, 0);
            notification = new NotificationCompat.Builder(this)
                    .setContentTitle(getResources().getString(R.string.notification_title))
                    .setTicker("This is a ticker")
                    .setStyle(new NotificationCompat.BigTextStyle().bigText(contentText))
                    .setContentText(contentText)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(icon)
                    .setChannelId(channelId)
                    .setContentIntent(piShowActivity) // back to activity
                    .setOngoing(true)
                    .addAction(android.R.drawable.ic_media_pause, App.getStringById(R.string.stop_mining), piStopMining)
                    .build();
        }

        return notification;
    }

    @Override
    public void networkAvailable() {

    }

    @Override
    public void networkUnavailable() {

    }

}

