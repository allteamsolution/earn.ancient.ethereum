package miner.bitcoin.tutorial;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.activity.MainActivity;
import miner.bitcoin.helper.AppPreferenceManager;
import miner.bitcoin.helper.MagicDrawable;
import miner.bitcoin.helper.UIHelper;
import miner.bitcoin.view.CustomViewPager;

import static miner.bitcoin.helper.MagicDrawable.createEnabled;

public class AuthActivity extends AppCompatActivity {

    private static final int FIRST_TAB_NUM = 0;
    private static final int SECOND_TAB_NUM = 1;
    private static final int THIRD_TAB_NUM = 2;
    private static final int FOURTH_TAB_NUM = 3;

    public static final String EXTRA = "EXTRA";
    private static final int INITIAL_TAB_INDEX = FIRST_TAB_NUM;

    @BindView(R.id.viewpager)
    public CustomViewPager mViewPager;

    @BindView(R.id.activity_tutorial)
    public View activity_tutorial;

    @BindView(R.id.ivLeft)
    public ImageView ivLeft;

    @BindView(R.id.ivRight)
    public ImageView ivRight;

    @BindView(R.id.tvDone)
    public TextView tvDone;

    private IndicatorController indicatorController;

    private MyAdapter adapter;

    private MagicDrawable rightArrow;
    private boolean refCodeApplyed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);

        indicatorController = new IndicatorController(activity_tutorial);

        ivLeft.setImageDrawable(createEnabled(App.getContext().getResources().getDrawable(R.drawable.ic_arrow_back_black_24dp),
                getResources().getColor(R.color.disabled_color),
                getResources().getColor(R.color.white)));

        rightArrow = createEnabled(App.getContext().getResources().getDrawable(R.drawable.ic_arrow_forward_black_24dp),
                getResources().getColor(R.color.disabled_color),
                getResources().getColor(R.color.white));
        ivRight.setImageDrawable(rightArrow);

        adapter = new MyAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(adapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                indicatorController.onPageScrolled(positionOffset, position);

                switch (position){
                    case FIRST_TAB_NUM:
                        UIHelper.hideKeyboard(AuthActivity.this);

                        tvDone.setVisibility(View.GONE);
                        ivRight.setVisibility(View.VISIBLE);
                        ivLeft.setVisibility(View.GONE);
                        break;
                    case SECOND_TAB_NUM:

                        tvDone.setVisibility(View.GONE);
                        ivRight.setVisibility(View.VISIBLE);
                        ivLeft.setVisibility(View.VISIBLE);

                        break;
                    case THIRD_TAB_NUM:
                        UIHelper.hideKeyboard(AuthActivity.this);

                        tvDone.setVisibility(View.GONE);
                        ivRight.setVisibility(View.VISIBLE);
                        ivLeft.setVisibility(View.VISIBLE);

                        break;

                    case FOURTH_TAB_NUM:
                        UIHelper.hideKeyboard(AuthActivity.this);

                        tvDone.setVisibility(View.VISIBLE);
                        ivRight.setVisibility(View.GONE);
                        ivLeft.setVisibility(View.VISIBLE);

                        break;
                }
            }

            @Override
            public void onPageSelected(int position) {}

            @Override
            public void onPageScrollStateChanged(int state) {}
        });

        mViewPager.setCurrentItem(INITIAL_TAB_INDEX);
    }

    public void refCodeApplyied(boolean applyed){
        refCodeApplyed = applyed;
    }

    public void onExitTutorial(){

        AppPreferenceManager.getInstance().setNotFirstLaunch();

        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in_at_once, R.anim.fade_out_at_once);
        finish();
    }

    public void onActivatePaging(boolean activate){
//        ivLeft.setEnabled(activate);
        ivRight.setEnabled(activate);
        mViewPager.setPagingEnabled(activate);
    }

    @OnClick(R.id.tvDone)
    public void onDoneClicked(){
        onExitTutorial();
    }

    @OnClick(R.id.ivLeft)
    public void onLeftClicked(){
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() - 1, true);

        onActivatePaging(true);
    }

    @OnClick(R.id.ivRight)
    public void onRightClicked(){
        mViewPager.setCurrentItem(mViewPager.getCurrentItem() + 1, true);
    }

    public static class MyAdapter extends FragmentStatePagerAdapter {

        private static final int SIZE = 4;

        private SparseArray<TutorialItemFragment> list = new SparseArray<>(SIZE);


        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TutorialItemFragment fragment = (TutorialItemFragment) super.instantiateItem(container, position);
            list.put(position, fragment);
            return fragment;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            list.remove(position);
            super.destroyItem(container, position, object);
        }

        @Override
        public int getCount() {
            return SIZE;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0:
                    if (list.get(0) == null){
                        list.put(0, TutorialZeroItem.newInstance());
                    }
                    return list.get(0);

                case 1:
                    if (list.get(1) == null){
                        list.put(1, TutorialFirstItem.newInstance());
                    }
                    return list.get(1);

                case 2:
                    if (list.get(2) == null){
                        list.put(2, TutorialSecondItem.newInstance());
                    }
                    return list.get(2);

                case 3:
                    if (list.get(3) == null){
                        list.put(3, TutorialThirdFragment.newInstance());
                    }
                    return list.get(3);
            }

            return null;
        }
    }

}
