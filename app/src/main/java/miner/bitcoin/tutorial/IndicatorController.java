package miner.bitcoin.tutorial;

import android.view.View;
import android.widget.FrameLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import miner.bitcoin.R;
import miner.bitcoin.helper.UIHelper;

public class IndicatorController {

    @BindView(R.id.indicator)
    public View indicator;

    public IndicatorController(View parent){
        ButterKnife.bind(this, parent);
    }

    public void onPageScrolled(float positionOffset, int position){
        final FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) indicator.getLayoutParams();
        params.leftMargin = (int)((positionOffset + position) * UIHelper.getPixel(18f));
        indicator.requestLayout();
    }

}
