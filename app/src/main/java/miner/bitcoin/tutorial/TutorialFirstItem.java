package miner.bitcoin.tutorial;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import miner.bitcoin.R;

public class TutorialFirstItem extends TutorialItemFragment {

    public static TutorialFirstItem newInstance(){
        TutorialFirstItem instance = new TutorialFirstItem();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.tutorial_fisrt, container, false);
        ButterKnife.bind(this, mRootView);

        return mRootView;
    }

}
