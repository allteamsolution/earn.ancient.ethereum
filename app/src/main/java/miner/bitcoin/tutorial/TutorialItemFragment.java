package miner.bitcoin.tutorial;

import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;

public abstract class TutorialItemFragment extends Fragment {

    protected ViewGroup mRootView;

    public View getRootView() {
        return mRootView;
    }
}
