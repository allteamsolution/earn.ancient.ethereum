package miner.bitcoin.tutorial;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import miner.bitcoin.R;
import miner.bitcoin.helper.Constants;

public class TutorialThirdFragment extends TutorialItemFragment {

    public static final String MARKET_URL_PREFIX = "market://details?id=";
    public static final String MARKET_BROWSER_URL_PREFIX = "https://play.google.com/store/apps/details?id=";

    public String pack;

    @BindView(R.id.btnInstall)
    public TextView btnInstall;

    public static TutorialThirdFragment newInstance(){
        TutorialThirdFragment instance = new TutorialThirdFragment();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.tutorial_third, container, false);
        ButterKnife.bind(this, mRootView);

        pack = Constants.PACK;
        Log.d("custompropery", "onCreateView: " + pack);
//        if (!TextUtil.isEmpty(AwsApp.getServerConfig().customPropery)) {
//            pack = AwsApp.getServerConfig().customPropery;
//        }

        return mRootView;
    }

    @OnClick(R.id.btnInstall)
    public void onBtnInstallClick(){
        if (getActivity() != null) {
            try {
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_URL_PREFIX + pack)));
            } catch (android.content.ActivityNotFoundException anfe) {
                getActivity().startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(MARKET_BROWSER_URL_PREFIX + pack)));
            }
        }
    }

    @OnClick(R.id.btnNext)
    public void onBtnNextClick() {
        ((AuthActivity) getActivity()).onDoneClicked();
    }

}

