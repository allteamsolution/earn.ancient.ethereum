package miner.bitcoin.tutorial;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import miner.bitcoin.App;
import miner.bitcoin.R;
import miner.bitcoin.helper.Utility;

public class TutorialZeroItem extends TutorialItemFragment {

    @BindView(R.id.cbPrivacy)
    protected CheckBox cbPrivacy;

    @BindView(R.id.sign_in_button)
    SignInButton sign_in_button;

    private AuthActivity activity;

    public static TutorialZeroItem newInstance() {
        TutorialZeroItem instance = new TutorialZeroItem();
        return instance;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = (ViewGroup) LayoutInflater.from(getContext()).inflate(R.layout.tutorial_zero, container, false);
        ButterKnife.bind(this, mRootView);

        cbPrivacy.setButtonDrawable(Utility.getCheckBoxDrawable());
        initPrivacy();

        init();

        sign_in_button.setVisibility(View.GONE);

        return mRootView;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        activity = (AuthActivity) context;
    }

    private void initPrivacy() {
        String str0 = App.getContext().getResources().getString(R.string.agree_privacy);
        String str1 = App.getContext().getResources().getString(R.string.agree_privacy_suff);

        ForegroundColorSpan spanWhite = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.white));
        ForegroundColorSpan spanFab = new ForegroundColorSpan(App.getContext().getResources().getColor(R.color.fabcolor));

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                String url = getResources().getString(R.string.privacy_url);
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(false);
            }
        };

        int size = str0.length();
        str0 += " " + str1;
        Spannable spannable = new SpannableString(str0);
        spannable.setSpan(spanWhite, 0, size, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        spannable.setSpan(clickableSpan, size, str0.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(spanFab, size, str0.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        cbPrivacy.setText(spannable);
        cbPrivacy.setMovementMethod(LinkMovementMethod.getInstance());
        cbPrivacy.setHighlightColor(Color.TRANSPARENT);
        cbPrivacy.setChecked(true);
    }

    @OnCheckedChanged(R.id.cbPrivacy)
    public void cbPrivacyChechChange() {
        if (cbPrivacy.isChecked() && App.getCurrentUser().signIn) {
            activity.onActivatePaging(true);
        } else {
            activity.onActivatePaging(false);
        }
    }

    private void init() {
        Log.d("HHEDHEFJ", "init");

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        activity.onActivatePaging(true);
    }

}

