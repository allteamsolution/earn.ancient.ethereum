package miner.bitcoin.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import miner.bitcoin.R;
import miner.bitcoin.provider.FaqProvider;

public class Constants {

    public static final int NOTIFICATION_CODE = 35;

    public static double minPayoutCrypt = 50;//ETH

    public static double satoshiToBtc = 0.001 * 0.001;//1 Gwei
    public static double btcToSatoshi = 1000 * 1000;//1 ETH

    public final static String CRYPT = "Ripple";
    public final static String CRYPT_CODE = "XMR";
    public final static String CRYPT_CODE_PARTS = "uXMR";

    public final static int referralBonus = 10000;//"uXMR"

    public static final float DEF_PLAN_REW = 0.000021f;
    public static final float PREMIUM_PLAN_REW = 0.000039f;
    public static final float PLATINUM_PLAN_REW = 0.000063f;

    public static double getReward(){
        return new Random().nextInt(5000) + 4000;
    }

    public static final String FORMAT_BALANCE = "%.6f";

    public static final float PREMIUM_PRICE = 17f;
    public static final float PLATINUM_PRICE = 30f;

    public static List<FaqProvider.FaqModel> getExtraFaqs(){
        return null;
    }
}
